# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-17 12:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != "
"11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % "
"100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || "
"(n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: authc/models.py:14 authc/models.py:19 authc/models.py:24 authc/models.py:30
#: authc/models.py:36
#, python-format
msgid "%(value)s is not a valid username"
msgstr ""

#: authc/models.py:44
msgid ""
"Enter a valid username. This value may contain only letters, numbers, and ./"
"+/-/_ characters."
msgstr ""

#: logistica/models/inventory.py:80
msgid "inventories"
msgstr ""

#: logistica/models/person.py:470 logistica/models/person.py:472
msgid "role and disambiguation role should be the same."
msgstr ""

#: logistica/models/person.py:478
msgid "A Person record cannot be inside region with level greater than 1."
msgstr ""

#: logistica/models/region.py:132
msgid "regions"
msgstr ""

#: logistica/models/region.py:140 logistica/models/region.py:142
msgid "Level greater than 1 cannot have postal code."
msgstr ""

#: logistica/models/request.py:52
msgid "requests"
msgstr ""

#: logistica/models/resource.py:73
msgid "resources"
msgstr ""

#: logistica/models/shipping.py:102
msgid "shippings"
msgstr ""

#: logistica/models/shipping.py:111 logistica/models/shipping.py:113
msgid "Resource of inventories mismatch."
msgstr ""

#: logistica/models/shipping.py:188 logistica/models/shipping.py:190
msgid "Inventory resource should be the same as resource."
msgstr ""

#: logistica/models/shipping.py:246
msgid "tracking"
msgstr ""

#: templates/logisticamail/activation.html:4
#, python-format
msgid "Account activation on %(site_name)s"
msgstr ""

#: templates/logisticamail/activation.html:8
#: templates/logisticamail/activation.html:19
#, python-format
msgid ""
"You're receiving this email because you need to finish activation process on "
"%(site_name)s."
msgstr ""

#: templates/logisticamail/activation.html:10
#: templates/logisticamail/activation.html:21
msgid "Please go to the following page to activate account:"
msgstr ""

#: templates/logisticamail/activation.html:13
#: templates/logisticamail/activation.html:24
#: templates/logisticamail/confirmation.html:10
#: templates/logisticamail/confirmation.html:18
#: templates/logisticamail/password_changed_confirmation.html:10
#: templates/logisticamail/password_changed_confirmation.html:18
#: templates/logisticamail/password_reset.html:15
#: templates/logisticamail/password_reset.html:27
#: templates/logisticamail/username_change_confirmation.html:10
#: templates/logisticamail/username_change_confirmation.html:18
#: templates/logisticamail/username_reset.html:14
#: templates/logisticamail/username_reset.html:26
msgid "Thanks for using our site!"
msgstr ""

#: templates/logisticamail/activation.html:15
#: templates/logisticamail/activation.html:26
#: templates/logisticamail/confirmation.html:12
#: templates/logisticamail/confirmation.html:20
#: templates/logisticamail/password_changed_confirmation.html:12
#: templates/logisticamail/password_changed_confirmation.html:20
#: templates/logisticamail/password_reset.html:17
#: templates/logisticamail/password_reset.html:29
#: templates/logisticamail/username_change_confirmation.html:12
#: templates/logisticamail/username_change_confirmation.html:20
#: templates/logisticamail/username_reset.html:16
#: templates/logisticamail/username_reset.html:28
#, python-format
msgid "The %(site_name)s team"
msgstr ""

#: templates/logisticamail/confirmation.html:4
#, python-format
msgid ""
"%(site_name)s - Your account has been successfully created and activated!"
msgstr ""

#: templates/logisticamail/confirmation.html:8
#: templates/logisticamail/confirmation.html:16
msgid "Your account has been created and is ready to use!"
msgstr ""

#: templates/logisticamail/password_changed_confirmation.html:4
#, python-format
msgid "%(site_name)s - Your password has been successfully changed!"
msgstr ""

#: templates/logisticamail/password_changed_confirmation.html:8
#: templates/logisticamail/password_changed_confirmation.html:16
msgid "Your password has been changed!"
msgstr ""

#: templates/logisticamail/password_reset.html:5
#, python-format
msgid "Password reset on %(site_name)s"
msgstr ""

#: templates/logisticamail/password_reset.html:9
#: templates/logisticamail/password_reset.html:21
#, python-format
msgid ""
"You're receiving this email because you requested a password reset for your "
"user account at %(site_name)s."
msgstr ""

#: templates/logisticamail/password_reset.html:11
#: templates/logisticamail/password_reset.html:23
msgid "Please go to the following page and choose a new password:"
msgstr ""

#: templates/logisticamail/password_reset.html:13
#: templates/logisticamail/password_reset.html:25
#: templates/logisticamail/username_reset.html:12
#: templates/logisticamail/username_reset.html:24
msgid "Your username, in case you've forgotten:"
msgstr ""

#: templates/logisticamail/username_change_confirmation.html:4
#, python-format
msgid "%(site_name)s - Your username has been successfully changed!"
msgstr ""

#: templates/logisticamail/username_change_confirmation.html:8
#: templates/logisticamail/username_change_confirmation.html:16
msgid "Your username has been changed!"
msgstr ""

#: templates/logisticamail/username_reset.html:4
#, python-format
msgid "Username reset on %(site_name)s"
msgstr ""

#: templates/logisticamail/username_reset.html:8
#: templates/logisticamail/username_reset.html:20
#, python-format
msgid ""
"You're receiving this email because you requested a username reset for your "
"user account at %(site_name)s."
msgstr ""

#: templates/logisticamail/username_reset.html:10
#: templates/logisticamail/username_reset.html:22
msgid "Please go to the following page and choose a new username:"
msgstr ""
