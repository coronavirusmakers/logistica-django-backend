import ssl
import smtplib
from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend
from djoser import email as djoser_email


class ForceTLS1SMTP(smtplib.SMTP):
    """Wrap of smtplib.SMTP wich forces TLSv1 protocol by default in his
     starttls context. If a context is passed manually to starttls(), this
     behavior gets overrided.
    """

    def starttls(self, keyfile=None, certfile=None, context=None):
        if context is None:
            context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        return super().starttls(
            keyfile=keyfile, certfile=certfile, context=context)


class ForceTLS1EmailBackend(EmailBackend):
    """Same backend as django.core.mail.backends.smtp.EmailBackend except that
    this backend forces the use of TLSv1.0 by default for those old mail servers
    that cannot use TLSv1.2
    """

    @property
    def connection_class(self):
        return smtplib.SMTP_SSL if self.use_ssl else ForceTLS1SMTP


class EmailTemplateContextOverride:
    def get_context_data(self):

        djoser_urls_attribute_name = getattr(
            self,
            'DJOSER_URLS_ATTRIBUTE_NAME',
            None,
        )
        if not djoser_urls_attribute_name:
            return super().get_context_data()

        context = super().get_context_data()
        # Asumimos que llegamos aquí siempre desde un request, no se sportan
        # otro tipo de llamadas. Se asume que el frontend envía
        # un header para identificar qué tipo de frontend es
        context_front_key = self.request.META.get('HTTP_CONTEXT_FRONT')
        raw_url = settings.DJOSER.get(djoser_urls_attribute_name).get(
            context_front_key)
        context["url"] = raw_url.format(**context)
        context_front = settings.CONTEXT_FRONTS.get(context_front_key)
        context["protocol"] = context_front.get('PROTOCOL')
        context["domain"] = context_front.get('DOMAIN')
        if context_front.get('PORT'):
            context["domain"] = context["domain"] + ":" + context_front.get(
                'PORT')
        context["basepath"] = context_front.get('BASEPATH')
        return context


class ActivationEmail(EmailTemplateContextOverride,
                      djoser_email.ActivationEmail):
    template_name = "logisticamail/activation.html"
    DJOSER_URLS_ATTRIBUTE_NAME = 'ACTIVATION_URLS'


class ConfirmationEmail(EmailTemplateContextOverride,
                        djoser_email.ConfirmationEmail):
    template_name = "logisticamail/confirmation.html"


class PasswordResetEmail(EmailTemplateContextOverride,
                         djoser_email.PasswordResetEmail):
    template_name = "logisticamail/password_reset.html"
    DJOSER_URLS_ATTRIBUTE_NAME = 'PASSWORD_RESET_CONFIRM_URLS'


class PasswordChangedConfirmationEmail(
        EmailTemplateContextOverride,
        djoser_email.PasswordChangedConfirmationEmail):
    template_name = "logisticamail/password_changed_confirmation.html"


class UsernameChangedConfirmationEmail(
        EmailTemplateContextOverride,
        djoser_email.UsernameChangedConfirmationEmail):
    template_name = "logisticamail/username_changed_confirmation.html"


class UsernameResetEmail(EmailTemplateContextOverride,
                         djoser_email.UsernameResetEmail):
    template_name = "logisticamail/username_reset.html"
    DJOSER_URLS_ATTRIBUTE_NAME = 'USERNAME_RESET_CONFIRM_URLS'
