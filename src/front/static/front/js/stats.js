$(window).ready(function() {
  loadByDayResourceChart();
  loadByDaySumResourceChart();
});

const addByDayResourceChart = function(id, title, labels, datasets) {
  $("#by-day-resource-stats").append(
    '<div class="col-xs-12 col-lg-6"><h3>' +
      title +
      '</h3><canvas id="' +
      id +
      '"></canvas></div>'
  );

  var ctx = document.getElementById(id);

  var byDayChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: labels,
      datasets: datasets
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });
};

const addByDaySumResourceChart = function(id, title, labels, datasets) {
  $("#by-day-sum-resource-stats").append(
    '<div class="col-xs-12 col-lg-6"><h3>' +
      title +
      '</h3><canvas id="' +
      id +
      '"></canvas></div>'
  );

  var ctx = document.getElementById(id);

  var byDayChart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: labels,
      datasets: datasets
    },
    options: {
      plugins: {
        colorschemes: {
          scheme: "brewer.Paired12"
        }
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });
};

const loadByDayResourceChart = function() {
  fetch(
    base_api_url +
      "/logistica/stats/resource/bydayresources/?from_date=" +
      "2020-01-01",
    {
      headers: {
        Accept: "application/json",
        "Content-type": "application/json"
      }
    }
  ).then(res => {
    res.json().then(data => {
      if (res.ok) {
        let labels = [];
        let datasets = [];
        data.results.forEach(resource => {
          resource.stats_by_region.forEach(region => {
            labels = region.stats.build_static_labels;
            datasets = [
              {
                label: "Fabricado",
                data: region.stats.build_static_values
              },
              {
                label: "Entregado",
                data: region.stats.deliver_static_values
              }
            ];
            addByDayResourceChart(
              "resource-" + resource.pk + "region-" + region.region.pk,
              region.region.name + " " + resource.name,
              labels,
              datasets
            );
          });
        });
      } else {
        // error
      }
    });
  });
};

const loadByDaySumResourceChart = function() {
  fetch(base_api_url + "/logistica/stats/resource/bydaysumresources/", {
    headers: {
      Accept: "application/json",
      "Content-type": "application/json"
    }
  }).then(res => {
    res.json().then(data => {
      if (res.ok) {
        let labels = [];
        let values = [];
        data.results.forEach(resource => {
          labels.push(resource.name + " (construidos)");
          values.push(resource.stats_sum.build_static_value);
          labels.push(resource.name + " (entregados)");
          values.push(resource.stats_sum.deliver_static_value);
        });
        addByDaySumResourceChart("by-day-totals", "Totales", labels, [
          {
            label: "Totales",
            data: values
          }
        ]);
      } else {
        // error
      }
    });
  });
};
