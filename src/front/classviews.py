from django.views.generic.base import TemplateView


class StatsView(TemplateView):
    template_name = "front/stats.html"
