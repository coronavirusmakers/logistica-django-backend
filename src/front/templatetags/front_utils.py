# import datetime
from django import template

register = template.Library()  # pylint: disable=invalid-name


# @register.simple_tag(takes_context=True)
@register.simple_tag()
def api_uri():
    return '/apiv1'
    # Esto tiene problemas cuando estamos detras de un load balancer,
    # no coloca el https
    # return context['request'].build_absolute_uri('/apiv1')
