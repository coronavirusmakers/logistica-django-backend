import logging
from django.db.models.signals import post_save
from django.dispatch import receiver
from logistica.models import (
    Inventory,
    AutoMovementConfiguration,
    Shipping,
    Region,
)

logger = logging.getLogger('logistica.signals')  # pylint: disable=invalid-name


def _create_automovement_shipping(automovement, inventory):
    # Evitamos hacer movimientos entre inventarios distintos
    if inventory.resource != automovement.target_inventory.resource:
        return

    # Obtenemos el carrier a usar, None si así se ha indicado
    try:
        carrier = automovement.carrier
    except AutoMovementConfiguration.carrier.RelatedObjectDoesNotExist:
        # En la configuración automática no se ha indicado
        # transportista, default None
        carrier = None

    # Crea el shipping de inventario (a) hacia inventario (b),
    # lo que significa que es un movimiento entre inventarios
    # y no un envío hacia un pedido
    Shipping.objects.create(
        inventory=inventory,
        move=automovement.target_inventory,
        carrier=carrier,
        quantity=inventory.quantity,
    )


@receiver(post_save, sender=Inventory)
def handle_auto_movements(instance=None, created=None, **_kwargs):
    if created:
        # Para este código se asume que un owner siempre va a tener
        # una región y que un inventario siempre va a tener un owner.
        # Si por algún motivo en los modelos esto no fuera así, este
        # código fallaría al no comprobar esos casos.
        region = instance.owner.region
        panic_exit_loop = 0
        while True:  # WARNING: infinite loop
            panic_exit_loop += 1
            if panic_exit_loop > 100:
                logger.warning("Automovement signal killed after loop 100")
                break
            try:
                automovement = AutoMovementConfiguration.objects.get(
                    source_region=region,
                    resource=instance.resource,
                )
                _create_automovement_shipping(automovement, instance)
                break
            except AutoMovementConfiguration.DoesNotExist:
                try:
                    region = region.parent
                    if not region:
                        break
                except Region.parent.RelatedObjectDoesNotExist:
                    break
                except AttributeError:
                    break
