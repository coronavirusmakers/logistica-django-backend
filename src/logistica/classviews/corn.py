from django.http import Http404
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from logistica import models
from logistica.serializers import corn as serializers


class CornShippingL(generics.ListAPIView):
    """
    get:
    Listar un Shippings que puede ver un transportista
    """
    serializer_class = serializers.CornShippingSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Shipping.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_fields = ('status', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Shipping.objects.none()
        # Acceso sólo a aquellas regiones a las que tiene acceso
        # el person indicado
        return models.Shipping.objects.filter(
            carrier__pk=self.kwargs.get('person_pk'),
            carrier__user=self.request.user,
        )


class CornShippingRU(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Shipping.objects.all()
    serializer_class = serializers.CornShippingSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        try:
            obj = models.Shipping.objects.get(
                pk=self.kwargs.get('shipping_pk'),
                carrier__pk=self.kwargs.get('person_pk'),
                carrier__user=self.request.user,
            )
        except models.Shipping.DoesNotExist:
            raise Http404()
        return obj
