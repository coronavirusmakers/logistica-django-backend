from django.db.models import Q
from django.db import transaction
from django.http import Http404
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated, BasePermission
from rest_framework.views import APIView
from rest_framework.filters import OrderingFilter
from rest_framework.exceptions import APIException
from django_filters.rest_framework import FilterSet, filters, DjangoFilterBackend

from logistica.models import (
    RoleDisambiguation,
    Person,
    Inventory,
    Resource,
    Region,
    Shipping,
    Request,
)
from logistica.serializers.basic import (
    BasicRoleDisambiguationSerializer,
    BasicPersonSerializer,
    BasicInventorySerializer,
    BasicResourceSerializer,
    BasicRegionSerializer,
    BasicRequestSerializer,
    BasicShippingSerializer,
)
from logistica.filters import RegionFilterSet


class ResponseForbidden(APIException):
    status_code = 403
    default_detail = 'Forbidden'
    default_code = 'forbidden'


class BasicPersonLC(generics.ListCreateAPIView):
    """
    get:
    Listar Persons de cualquier tiepo
    post:
    Crear un Person de cualquier tipo
    """
    serializer_class = BasicPersonSerializer
    permission_classes = (IsAuthenticated, )
    queryset = Person.objects.all()
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('role', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return Person.objects.none()
        return Person.objects.filter(
            user=self.request.user,
            # role=Person.ROLE_PRODUCER,
        )


class BasicPersonRUD(generics.RetrieveUpdateDestroyAPIView):
    queryset = Person.objects.all()
    serializer_class = BasicPersonSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return Person.objects.get(
            pk=self.kwargs.get('person_pk'),
            user=self.request.user,
        )


# TODO: Añadir ordering
class BasicInventoryLC(generics.ListCreateAPIView):
    """
    get: Listar productos disponibles de un person
    post: crear productos en el inventario de un person
    """

    class BasicInventoryFilterSet(FilterSet):
        resource_type = filters.NumberFilter(method='filter_resource', )

        def filter_resource(self, queryset, _name, value):
            return queryset.filter(resource__resource_type=value)

    serializer_class = BasicInventorySerializer
    permission_classes = (IsAuthenticated, )
    queryset = Inventory.objects.all()
    filterset_class = BasicInventoryFilterSet
    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            Inventory.objects.none()
        return Inventory.objects.filter(
            owner__user=self.request.user,
            owner__pk=self.kwargs.get('person_pk'),
        )


class BasicInventoryRUD(generics.RetrieveUpdateDestroyAPIView):
    class PreventUpdateAlreadyShippedInventory(BasePermission):
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):
            if request.method == 'GET':
                return True
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                # Evitar modificaciones si este inventario
                # ha sido asociado a un envío que no ha sido
                # cancelado
                in_use_request_shippings = obj.shippings.filter(
                    Q(~Q(status__in=[
                        Shipping.STATUS_AWAITING, Shipping.STATUS_CANCELLED
                    ])))
                in_use_movements_shippings = obj.shippings.filter(
                    Q(~Q(status__in=[
                        Shipping.STATUS_AWAITING, Shipping.STATUS_CANCELLED
                    ])))
                if not in_use_request_shippings and not in_use_movements_shippings:
                    return True
                return False
            return False

    queryset = Inventory.objects.all()
    serializer_class = BasicInventorySerializer
    permission_classes = (
        IsAuthenticated,
        PreventUpdateAlreadyShippedInventory,
    )

    def get_object(self):
        obj = Inventory.objects.get(
            pk=self.kwargs.get('inventory_pk'),
            owner__user=self.request.user,
        )
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_destroy(self, instance):
        in_use_request_shippings = instance.shippings.filter(
            Q(~Q(status__in=[
                Shipping.STATUS_AWAITING, Shipping.STATUS_CANCELLED
            ])))
        in_use_movements_shippings = instance.shippings.filter(
            Q(~Q(status__in=[
                Shipping.STATUS_AWAITING, Shipping.STATUS_CANCELLED
            ])))

        if not in_use_request_shippings and not in_use_movements_shippings:
            with transaction.atomic():
                # Disasociate
                instance.shippings.all().update(
                    status=Shipping.STATUS_CANCELLED,
                    inventory=None,
                )
                instance.shippings_in.all().update(
                    status=Shipping.STATUS_CANCELLED,
                    inventory=None,
                )
                instance.delete()
        else:
            raise ResponseForbidden()


class BasicResourceL(generics.ListAPIView):
    """
    get: listar recursos (definición de producto)
        resource_type
        TYPE_MAKER_MATERIAL = 0
        TYPE_PRODUCT_PART = 1
        TYPE_END_PRODUCT = 2
    """
    serializer_class = BasicResourceSerializer
    permission_classes = (AllowAny, )
    queryset = Resource.objects.all()
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('resource_type', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            Inventory.objects.none()
        return self.queryset


class BasicResourceR(generics.RetrieveAPIView):
    """
        get: obtiene un recurso
    """
    queryset = Resource.objects.all()
    serializer_class = BasicResourceSerializer
    permission_classes = (AllowAny, )
    lookup_url_kwarg = 'resource_pk'


class BasicRegionL(generics.ListAPIView):
    """
    get: listar regiones. Los persons se organizan por regiones
    """

    serializer_class = BasicRegionSerializer
    permission_classes = (AllowAny, )
    queryset = Region.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = RegionFilterSet

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            Region.objects.none()
        # Basic region list can only list level 1 regions
        return Region.objects.filter(level=1)


class BasicRoleL(APIView):
    """
    get: listado de roles disponibles
    """
    permission_classes = (AllowAny, )

    def get(
            self,
            request,  # pylint: disable=unused-argument
            format=None,  # pylint: disable=unused-argument,redefined-builtin
    ):
        roles = list()
        for fake_pk, role in Person.ROLE_CHOICES:
            roles.append({
                'pk': fake_pk,
                'name': role,
            })
        return Response({'results': roles})


class BasicRoleDisambiguationL(generics.ListAPIView):
    """
    get: Información más precisa de los subtipos disponibles para un role
    """
    serializer_class = BasicRoleDisambiguationSerializer
    permission_classes = (AllowAny, )
    queryset = RoleDisambiguation.objects.all()

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return RoleDisambiguation.objects.none()
        return RoleDisambiguation.objects.filter(
            role=self.kwargs.get('role_id'))


class BasicRequestLC(generics.ListCreateAPIView):
    """
    get:
    Listar Requests
    post:
    Crear un Request
    """
    serializer_class = BasicRequestSerializer
    permission_classes = (IsAuthenticated, )
    queryset = Request.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_fields = (
        'status',
        'consumer__role',
    )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return Request.objects.none()
        return Request.objects.filter(
            consumer__pk=self.kwargs.get('person_pk'),
            consumer__user=self.request.user,
        )


class BasicRequestRU(generics.RetrieveUpdateAPIView):
    class AllowCancelRequest(BasePermission):
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):  # pylint: disable=too-many-return-statements
            if request.method == 'GET':
                return False

            if request.method in ['PUT', 'DELETE', 'POST']:
                return False

            # Permitir hacer PATCH para setear estado a cancelado
            #  si el pedido no tiene ningún shipping activo
            if request.method == 'PATCH':
                person_pk = view.kwargs.get('person_pk', None)
                if person_pk is None:
                    return False
                person_pk = int(person_pk)

                to_set_status = request.data.get('status', None)
                if to_set_status is None:
                    return False
                to_set_status = int(to_set_status)

                if to_set_status != Request.STATUS_CANCELLED:
                    return False

                in_use_request_shippings = obj.shippings.filter(
                    Q(~Q(status__in=[
                        Shipping.STATUS_AWAITING, Shipping.STATUS_CANCELLED
                    ])))

                if in_use_request_shippings:
                    return False
                return True
            return False

    queryset = Request.objects.all()
    serializer_class = BasicRequestSerializer
    permission_classes = (
        IsAuthenticated,
        AllowCancelRequest,
    )

    def get_object(self):
        try:
            obj = Request.objects.get(
                pk=self.kwargs.get('request_pk'),
                consumer__pk=self.kwargs.get('person_pk'),
                consumer__user=self.request.user,
            )
        except Request.DoesNotExist:
            raise Http404()
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_update(self, serializer):
        instance = serializer.save()
        instance.shippings.all().update(status=Shipping.STATUS_CANCELLED, )


class BasicShippingU(generics.UpdateAPIView):
    class AllowSetStatusOrNot(BasePermission):
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):  # pylint: disable=too-many-return-statements
            if request.method == 'GET':
                return False

            if request.method in ['PUT', 'DELETE', 'POST']:
                return False

            if request.method == 'PATCH':
                # Preámbulo, obtenemos las vars que vamos a necesitar
                can_set_sent = False
                can_set_received = False

                person_pk = view.kwargs.get('person_pk', None)
                if person_pk is None:
                    return False
                person_pk = int(person_pk)

                to_set_status = request.data.get('status', None)
                if to_set_status is None:
                    return False
                to_set_status = int(to_set_status)

                # Comprueba si el person puede setear SENT
                try:
                    inventory_owner = obj.inventory.owner
                    if (inventory_owner.can_set_send_status
                            and inventory_owner.pk == person_pk):
                        can_set_sent = True
                except Shipping.inventory.RelatedObjectDoesNotExist:
                    can_set_sent = False
                except AttributeError:
                    # Un envío podría venir sin inventory, captura y deniega
                    # el acceso
                    can_set_sent = False

                # Comprueba si el person puede setear RECEIVED
                try:
                    consumer = obj.request.consumer
                    if (consumer.can_set_received_status
                            and consumer.pk == person_pk):
                        can_set_received = True
                except Shipping.request.RelatedObjectDoesNotExist:
                    can_set_received = False
                except AttributeError:
                    # Un envío podría venir sin pedido en caso de
                    # movimiento de inventario, captura y marca
                    # como False
                    can_set_received = False

                # Testea lo que se quiere setear y si se puede
                if ((to_set_status == Shipping.STATUS_SENT and can_set_sent)
                        or (to_set_status == Shipping.STATUS_RECEIVED
                            and can_set_received)):
                    return True
                return False

            return False

    queryset = Shipping.objects.all()
    serializer_class = BasicShippingSerializer
    permission_classes = (
        IsAuthenticated,
        AllowSetStatusOrNot,
    )

    def get_object(self):
        try:
            # Person como dueño del inventario, se permite marcar
            # como enviado?
            obj = Shipping.objects.get(
                pk=self.kwargs.get('shipping_pk'),
                inventory__owner__pk=self.kwargs.get('person_pk'),
                inventory__owner__user=self.request.user,
            )
            if not obj.inventory.owner.can_set_send_status:
                raise ResponseForbidden()
        except Shipping.DoesNotExist:
            try:
                # Person como receptor del inventario, se permite marcar
                # como recibido?
                obj = Shipping.objects.get(
                    pk=self.kwargs.get('shipping_pk'),
                    request__consumer__pk=self.kwargs.get('person_pk'),
                    request__consumer__user=self.request.user,
                )
                if not obj.request.consumer.can_set_received_status:
                    raise ResponseForbidden()
            except Shipping.DoesNotExist:
                raise ResponseForbidden()

        self.check_object_permissions(self.request, obj)
        return obj

    def get_serializer_context(self):
        if getattr(self, 'swagger_fake_view', False):
            return super().get_serializer_context()
        try:
            person = Person.objects.get(
                pk=self.kwargs.get('person_pk'),
                user=self.request.user,
            )
        except Person.DoesNotExist:
            raise Http404()
        context = super().get_serializer_context()
        context.update({
            "person": person,
        })
        return context
