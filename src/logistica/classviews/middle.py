from io import BytesIO
from django.db.models import Q, Subquery, F
from django.http import Http404, HttpResponse
from django.utils.functional import cached_property
from django.db.models.functions import Coalesce
from django.core.files import File
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    BasePermission,
)
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from logistica import models
from logistica.serializers import middle as serializers
from logistica import filters as logistica_filters
from logistica import exports as logistica_exports


class BasePersonUtilsView():
    @cached_property
    def current_person(self):
        """Obtain the Person of the current request. This prop will be cached
        while the View Object remains instantiated.

        Raises:
            Http404: if not found

        Returns:
            Person: Instance of current person
        """
        try:
            person = models.Person.objects.get(
                pk=self.kwargs.get('person_pk'),
                user=self.request.user,
                role=models.Person.ROLE_LOGISTICS,
            )
            return person
        except models.Person.DoesNotExist:
            raise Http404()


class MiddlePersonLC(generics.ListCreateAPIView):
    """
    get:
    Listar un Persons de logística
    post:
    Crear un person de logística
    """
    serializer_class = serializers.MiddlePersonSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Person.objects.all()
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('role', )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Person.objects.none()
        return models.Person.objects.filter(
            user=self.request.user,
            role=models.Person.ROLE_LOGISTICS,
        )


class MiddlePersonRUD(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Person.objects.all()
    serializer_class = serializers.MiddlePersonSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return models.Person.objects.get(
            pk=self.kwargs.get('person_pk'),
            role=models.Person.ROLE_LOGISTICS,
            user=self.request.user,
        )


class MiddleRequestLC(generics.ListCreateAPIView, BasePersonUtilsView):
    """
    get:
    Listar un Requests de logística
    post:
    Crear un Requests de logística
    """

    serializer_class = serializers.MiddleRequestSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Request.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = logistica_filters.MiddleRequestFilterSet

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Request.objects.none()
        # Acceso sólo a aquellas regiones a las que tiene acceso
        # el person indicado
        regions_queryset = self.current_person.logistics_allowed_read_regions()
        # yapf: disable
        # Anota la cantidad sumada de cantidad en los shippings
        # excluyendo aquellos que han sido cancelados
        return models.Request.objects.filter(
            consumer__region__in=Subquery(regions_queryset.values('pk'))
        ).annotate(
            quantity_satisfied=Coalesce(logistica_filters.SumDistinctHACK(
                'shippings__quantity',
                distinct=True,
                filter=Q(
                    ~Q(shippings__status=models.Shipping.STATUS_CANCELLED)
                )
            ), 0),
        ).select_related(
            'consumer',
            'resource',
        ).prefetch_related(
            'consumer__region',
            'shippings',
            'shippings__inventory',
            'shippings__carrier',
            'shippings__carrier__region',
        ).distinct()
        # yapf: enable


class MiddleRequestRUD(generics.RetrieveUpdateDestroyAPIView,
                       BasePersonUtilsView):
    class PersonCanAccessRequest(BasePermission):
        # From DRF doc:
        # The instance-level has_object_permission method will only be called
        # if the view-level has_permission checks have already passed.
        def has_permission(self, request, view):
            return True

        # Accede a aquellos pedidos hechos desde personas
        # con región a la que tiene acceso el usuario
        def has_object_permission(self, request, view, obj):
            if request.method == 'GET':
                if view.current_person.logistica_can_read_region(
                        obj.consumer.region):
                    return True
                return False
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                if view.current_person.logistica_can_write_region(
                        obj.consumer.region):
                    return True
                return False
            return False

    queryset = models.Request.objects.all()
    serializer_class = serializers.MiddleRequestSerializer
    permission_classes = (
        IsAuthenticated,
        PersonCanAccessRequest,
    )

    def get_object(self):
        try:
            obj = models.Request.objects.get(pk=self.kwargs.get('request_pk'))
        except models.Request.DoesNotExist:
            raise Http404()
        self.check_object_permissions(self.request, obj)
        return obj


class MiddleRegionL(generics.ListAPIView, BasePersonUtilsView):
    """
    get: listar regiones que un person de logística puede gestionar
    """

    serializer_class = serializers.MiddleRegionSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Region.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = logistica_filters.RegionFilterSet

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Region.objects.none()
        return self.current_person.logistics_allowed_read_regions()


class MiddleRequestStatusL(APIView, BasePersonUtilsView):
    """
    get: Listado de estados disponibles para usar en un pedido
    """
    permission_classes = (AllowAny, )

    def get(
            self,
            request,  # pylint: disable=unused-argument
            format=None,  # pylint: disable=redefined-builtin,unused-argument
            person_pk=None,  # pylint: disable=unused-argument
    ):
        # Hará raise Http404 si el person indicado en la url no corresponde
        # al usuario logeado o no es de tipo logística
        self.current_person  # pylint: disable=pointless-statement

        statuses = list()
        for fake_pk, status in models.Request.STATUS_CHOICES:
            statuses.append({
                'pk': fake_pk,
                'name': status,
            })
        return Response({'results': statuses})


class MiddleShippingStatusL(APIView, BasePersonUtilsView):
    """
    get: Listado de estados disponibles para usar en un shipping
    """
    permission_classes = (AllowAny, )

    def get(
            self,
            request,  # pylint: disable=unused-argument
            format=None,  # pylint: disable=redefined-builtin,unused-argument
            person_pk=None,  # pylint: disable=unused-argument
    ):
        # Hará raise Http404 si el person indicado en la url no corresponde
        # al usuario logeado o no es de tipo logística
        self.current_person  # pylint: disable=pointless-statement

        statuses = list()
        for fake_pk, status in models.Shipping.STATUS_CHOICES:
            statuses.append({
                'pk': fake_pk,
                'name': status,
            })
        return Response({'results': statuses})


class MiddleInventoryLC(generics.ListCreateAPIView, BasePersonUtilsView):
    """
    get: Inventario disponible, de otros persons que este person puede ver.
     Podrá ver sólo aquellos a los que tenga acceso por región permitida.
    """

    serializer_class = serializers.MiddleInventorySerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Inventory.objects.all()
    filterset_class = logistica_filters.InventoryFilterSet
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )

    ordering_fields = (
        'pk',
        'owner__name',
        'owner__role__name',
        'owner__region__name',
        'resource__name',
        'quantity',
        'sum_quantity_available',
    )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Inventory.objects.none()
        regions_queryset = self.current_person.logistics_allowed_read_regions()
        # yapf: disable
        return models.Inventory.objects.filter(
            owner__region__in=Subquery(regions_queryset.values('pk'))
        ).select_related(
            'resource', 'owner',
        ).prefetch_related('owner__region').annotate(
            # Movimientos de otros inventarios a este inventario
            sum_movements_to_this=Coalesce(logistica_filters.SumDistinctHACK(
                'shippings_in__quantity',
                distinct=True,
                filter=Q(
                    shippings_in__status__in=[
                        models.Shipping.STATUS_SENT,
                        models.Shipping.STATUS_RECEIVED,
                        models.Shipping.STATUS_SCHEDULED,
                        models.Shipping.STATUS_IN_TRANSIT,
                    ]
                )
            ), 0),
            # Cantidad consumida por pedidos
            sum_request_consumed=Coalesce(logistica_filters.SumDistinctHACK( # c
                'shippings__quantity',
                distinct=True,
                filter=Q(
                    shippings__status__in=[
                        models.Shipping.STATUS_SENT,
                        models.Shipping.STATUS_RECEIVED,
                        models.Shipping.STATUS_SCHEDULED,
                        models.Shipping.STATUS_IN_TRANSIT,
                    ]
                )
            ), 0),
            # Cantidad fija del inventario + movimientos al inventario - cantidad consumida por pedidos
            sum_quantity_available=F('quantity') + F('sum_movements_to_this') - F('sum_request_consumed'),
            # Cantidad en espera de ser movida a este inventario
            sum_awaiting_movements_to_this=Coalesce(logistica_filters.SumDistinctHACK(
                'shippings_in__quantity',
                distinct=True,
                filter=Q(shippings_in__status=models.Shipping.STATUS_AWAITING)
            ), 0),
            # Cantidad en espera de ser consumida por este inventario
            sum_awaiting_request_consumed=Coalesce(logistica_filters.SumDistinctHACK( # c
                'shippings__quantity',
                distinct=True,
                filter=Q(shippings__status=models.Shipping.STATUS_AWAITING)
            ), 0),
        ).distinct()
        # yapf: enable


class MiddleInventoryRUD(generics.RetrieveUpdateDestroyAPIView,
                         BasePersonUtilsView):
    class PersonCanAccessInventory(BasePermission):
        # From DRF doc:
        # The instance-level has_object_permission method will only be called
        # if the view-level has_permission checks have already passed.
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):
            if request.method == 'GET':
                if view.current_person.logistica_can_read_region(
                        obj.owner.region):
                    return True
                return False
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                if view.current_person.logistica_can_write_region(
                        obj.owner.region):
                    return True
                return False
            return False

    queryset = models.Inventory.objects.all()
    serializer_class = serializers.MiddleInventorySerializer
    permission_classes = (
        IsAuthenticated,
        PersonCanAccessInventory,
    )

    def get_object(self):
        try:
            obj = models.Inventory.objects.get(
                pk=self.kwargs.get('inventory_pk'))
        except models.Inventory.DoesNotExist:
            raise Http404()
        self.check_object_permissions(self.request, obj)
        return obj


class MiddleShippingLC(generics.ListCreateAPIView, BasePersonUtilsView):
    """
    get:
    Listar un Shippings de logística
    post:
    Crear un Shippings de logística
    """

    serializer_class = serializers.MiddleShippingSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Shipping.objects.all()
    filter_backends = (
        OrderingFilter,
        DjangoFilterBackend,
    )
    filterset_class = logistica_filters.MiddleShippingFilterSet
    ordering_fields = (
        'pk',
        'uuid',
        'status',
        'carrier__name',
        'request__consumer__name',
        'inventory__owner__name',
        'request',
        'quantity',
    )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Shipping.objects.none()
        # Acceso sólo a aquellas regiones a las que tiene acceso
        # el person indicado
        regions_queryset = self.current_person.logistics_allowed_read_regions()
        # yapf: disable
        return models.Shipping.objects.filter(Q(
            Q(request__consumer__region__in=Subquery(regions_queryset.values('pk')))
            | Q(move__owner__region__in=Subquery(regions_queryset.values('pk')))
            | Q(inventory__owner__region__in=Subquery(regions_queryset.values('pk')))
        )).select_related(
            'inventory',
            'carrier',
            'request',
            'move',
        ).prefetch_related(
            'request__consumer__region',
            'request__resource',
            'carrier__region',
            'inventory__owner__region',
            'inventory__resource',
            'move__resource',
            'move__owner__region',
        )
        # yapf: enable

    def list(self, request, *args, **kwargs):
        export = request.query_params.get('export')
        if export:
            queryset = self.filter_queryset(self.get_queryset())
            dataset = logistica_exports.ShippingResource().export(
                queryset=queryset)
            file = BytesIO(dataset.xls)
            file.name = 'shipping_export.xls'

            response = HttpResponse(
                File(file), content_type='application/vnd.ms-excel')
            response[
                'Content-Disposition'] = 'attachment; filename="%s"' % file.name
            return response

        return super().list(request, *args, **kwargs)

    # def list(self, request, *args, **kwargs):
    #     queryset = self.filter_queryset(self.get_queryset())

    #     page = self.paginate_queryset(queryset)
    #     if page is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)

    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)


class MiddleShippingRUD(generics.RetrieveUpdateDestroyAPIView,
                        BasePersonUtilsView):
    class PersonCanAccessShipping(BasePermission):
        # From DRF doc:
        # The instance-level has_object_permission method will only be called
        # if the view-level has_permission checks have already passed.
        def has_permission(self, request, view):
            return True

        def _test_movement(self, _request, view, obj):
            if view.current_person.logistica_can_read_region(
                    obj.move.owner.region):
                return True
            return False

        def _test_request(self, _request, view, obj):
            if view.current_person.logistica_can_read_region(
                    obj.request.consumer.region):
                return True
            return False

        def _test_inventory(self, _request, view, obj):
            if view.current_person.logistica_can_read_region(
                    obj.request.consumer.region):
                return True
            return False

        # Accede a aquellos pedidos hechos desde personas
        # con región a la que tiene acceso el usuario
        def has_object_permission(self, request, view, obj):
            # POST == Creation -> will be tested in serializer
            if request.method != 'POST':
                if obj.move:
                    return self._test_movement(request, view, obj)
                if obj.request:
                    return self._test_request(request, view, obj)
                if obj.inventory:
                    return self._test_inventory(request, view, obj)
                return False
            return True

    queryset = models.Shipping.objects.all()
    serializer_class = serializers.MiddleShippingSerializer
    permission_classes = (
        IsAuthenticated,
        PersonCanAccessShipping,
    )

    def get_object(self):
        try:
            obj = models.Shipping.objects.get(
                pk=self.kwargs.get('shipping_pk'))
        except models.Shipping.DoesNotExist:
            raise Http404()
        self.check_object_permissions(self.request, obj)
        return obj


class MiddleRequestShippingLC(MiddleShippingLC):
    serializer_class = serializers.MiddleRequestShippingSerializer

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Shipping.objects.none()
        queryset = super().get_queryset()
        return queryset.filter(request__pk=self.kwargs.get('request_pk'))


class MiddleRequestShippingRUD(MiddleShippingRUD):
    serializer_class = serializers.MiddleRequestShippingSerializer

    def get_object(self):
        try:
            obj = models.Shipping.objects.get(
                pk=self.kwargs.get('shipping_pk'),
                request__pk=self.kwargs.get('request_pk'))
        except models.Shipping.DoesNotExist:
            raise Http404()
        self.check_object_permissions(self.request, obj)
        return obj


class MiddleForeignPersonLC(generics.ListCreateAPIView, BasePersonUtilsView):
    """
    get:
    Listar Persons de todo tipo, limitado por región de acceso
    post:
    Crear un person de todo tipo, limitado por región de acceso
    """
    serializer_class = serializers.MiddleForeignPersonSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.Person.objects.all()
    filter_backends = (
        DjangoFilterBackend,
        OrderingFilter,
    )
    filterset_fields = (
        'role',
        'role_disambiguation',
    )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Person.objects.none()
        # Regiones permitidas para listar
        regions_queryset = self.current_person.logistics_allowed_read_regions()
        # roles permitidos para listar
        # excepto logística (no queremos que los usuarios se modifiquen entre si)
        roles_dict = dict(models.Person.ROLE_CHOICES)
        del roles_dict[models.Person.ROLE_LOGISTICS]
        roles = list(roles_dict.keys())

        return models.Person.objects.filter(
            role__in=roles,
            region__in=Subquery(regions_queryset.values('pk')),
        )


class MiddleForeignPersonRUD(generics.RetrieveUpdateDestroyAPIView,
                             BasePersonUtilsView):
    class PersonCanAccessShipping(BasePermission):
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):
            if request.method == 'GET':
                if view.current_person.logistica_can_read_region(obj.region):
                    return True
                return False
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                if view.current_person.logistica_can_write_region(obj.region):
                    return True
                return False
            return False

    queryset = models.Person.objects.all()
    serializer_class = serializers.MiddleForeignPersonSerializer
    permission_classes = (
        IsAuthenticated,
        PersonCanAccessShipping,
    )

    def get_object(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.Person.objects.none()
        regions_queryset = self.current_person.logistics_allowed_read_regions()
        roles = list(dict(models.Person.ROLE_CHOICES).keys())

        try:
            obj = models.Person.objects.get(
                pk=self.kwargs.get('foreignperson_pk'),
                role__in=roles,
                region__in=Subquery(regions_queryset.values('pk')),
            )
        except models.Person.DoesNotExist:
            raise Http404

        self.check_object_permissions(self.request, obj)
        return obj


class MiddleAutoMovementConfigurationBulkC(generics.CreateAPIView,
                                           BasePersonUtilsView):
    """
    post: Creación por lotes de movimientos automáticos
    """
    serializer_class = serializers.MiddleAutoMovementConfigurationBulkSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.AutoMovementConfiguration.objects.all()


class MiddleAutoMovementConfigurationL(generics.ListAPIView,
                                       BasePersonUtilsView):
    """
    get: Listado de los automatismos configurados
    """
    serializer_class = serializers.MiddleAutoMovementConfigurationSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.AutoMovementConfiguration.objects.all()

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.AutoMovementConfiguration.objects.none()
        # Regiones permitidas para listar
        regions_queryset = self.current_person.logistics_allowed_read_regions(
            include_parents=True)
        return models.AutoMovementConfiguration.objects.filter(
            source_region__in=Subquery(regions_queryset.values('pk')), )


class MiddleAutoMovementConfigurationRUD(generics.RetrieveUpdateDestroyAPIView,
                                         BasePersonUtilsView):
    """
    get: Obtener automatismo
    """

    class PersonCanAccessAutoMovement(BasePermission):
        def has_permission(self, request, view):
            return True

        def has_object_permission(self, request, view, obj):
            if request.method == 'GET':
                if view.current_person.logistica_can_read_region(
                        obj.source_region):
                    return True
                return False
            if request.method in ['PUT', 'PATCH', 'DELETE']:
                if view.current_person.logistica_can_write_region(
                        obj.source_region):
                    return True
                return False
            return False

    serializer_class = serializers.MiddleAutoMovementConfigurationSerializer
    permission_classes = (IsAuthenticated, )
    queryset = models.AutoMovementConfiguration.objects.all()

    def get_object(self):
        if getattr(self, 'swagger_fake_view', False):
            return models.AutoMovementConfiguration.objects.none()
        regions_queryset = self.current_person.logistics_allowed_read_regions(
            include_parents=True)

        try:
            obj = models.AutoMovementConfiguration.objects.get(
                pk=self.kwargs.get('automovementconfig_pk'),
                source_region__in=Subquery(regions_queryset.values('pk')),
            )
        except models.AutoMovementConfiguration.DoesNotExist:
            raise Http404

        self.check_object_permissions(self.request, obj)
        return obj
