from rest_framework import serializers
from logistica import models


class CornRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Region
        fields = (
            'pk',
            'name',
        )
        read_only_fields = fields


class CornPersonSerializer(serializers.ModelSerializer):
    region_object = CornRegionSerializer(read_only=True, source='region')

    class Meta:
        model = models.Person
        fields = (
            'pk',
            'role',
            'name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'phone',
            'point',
            'production_capacity',
            'logistics_need',
            'user',
            'can_make_orders',
        )
        read_only_fields = fields


class CornCarrierSerializer(serializers.ModelSerializer):
    region_object = CornRegionSerializer(read_only=True, source='region')

    class Meta:
        model = models.Person
        fields = (
            'pk',
            'role',
            'name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'phone',
            'point',
            'production_capacity',
            'user',
        )
        read_only_fields = fields


class CornResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Resource
        fields = (
            'pk',
            'resource_type',
            'name',
            'sku',
            'spl_link',
            'image_link',
        )
        read_only_fields = fields


class CornRequestSerializer(serializers.ModelSerializer):
    resource_object = CornResourceSerializer(
        read_only=True,
        source='resource',
    )
    consumer_object = CornPersonSerializer(read_only=True, source='consumer')

    class Meta:
        model = models.Request
        fields = (
            'pk',
            'status',
            'consumer',
            'consumer_object',
            'resource',
            'resource_object',
            'quantity',
            'done',
        )
        read_only_fields = fields


class CornInventorySerializer(serializers.ModelSerializer):
    quantity_available = serializers.IntegerField(read_only=True)
    auto_gen_batch = serializers.IntegerField(read_only=True)
    pad_batch = serializers.CharField(read_only=True)
    owner_object = CornPersonSerializer(read_only=True, source='owner')
    resource_object = CornResourceSerializer(
        read_only=True,
        source='resource',
    )

    class Meta:
        model = models.Inventory
        fields = (
            'pk',
            'resource',
            'resource_object',
            'owner',
            'owner_object',
            'quantity',
            'track_batch',
            'photo_link',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
        )
        read_only_fields = fields


class CornShippingSerializer(serializers.ModelSerializer):
    """Serializer de Shipping básico
    """
    inventory_object = CornInventorySerializer(
        read_only=True,
        source='inventory',
    )
    carrier_object = CornCarrierSerializer(
        read_only=True,
        source='carrier',
    )
    request_object = CornRequestSerializer(
        read_only=True,
        source='request',
    )

    class Meta:
        model = models.Shipping
        fields = (
            'pk',
            'uuid',
            'status',  # Este es el único campo que se puede modificar
            'inventory',
            'inventory_object',
            'carrier',
            'carrier_object',
            'carrier_track_num',
            'carrier_label_base64',
            'carrier_label_base64_format',
            'request',
            'request_object',
            'quantity',
        )
        read_only_fields = (
            'pk',
            'uuid',
            'inventory',
            'inventory_object',
            'carrier',
            'carrier_object',
            'carrier_track_num',
            'carrier_label_base64',
            'carrier_label_base64_format',
            'request',
            'request_object',
            'quantity',
        )
