import logging
from django.db import transaction
from django.conf import settings
from rest_framework import serializers
# from rest_framework_gis.serializers import GeometryField
from logistica import models

logger = logging.getLogger('middle.serializers')  # pylint: disable=invalid-name


class HandleRegionPermissions(serializers.ModelSerializer):
    def __obtain_region_from_path(self, path, target):
        region_path = target
        for subpath in path.split('__'):
            try:
                region_path = getattr(region_path, subpath)
            except AttributeError:
                try:
                    region_path = region_path.get(subpath)
                except AttributeError:
                    region_path = None
            if not region_path:
                if settings.DEBUG:
                    logger.warning("Cannot found region path %s for %s",
                                   subpath, path)
                    return None
        return region_path

    def _validate_readwrite_one_region_path(self, path, target,
                                            read_mode=True):
        region = self.__obtain_region_from_path(path, target)
        if not region:
            return
        if self.context.get('view') is None:
            raise ValueError('Cannot get view instance')

        person = self.context.get('view').current_person
        if read_mode:
            if not person.logistica_can_read_region(region):
                raise serializers.ValidationError(
                    {
                        'region': 'Permission denied'
                    },
                    code='denied',
                )
        else:
            if not person.logistica_can_write_region(region):
                raise serializers.ValidationError(
                    {
                        'region': 'Permission denied'
                    },
                    code='denied',
                )

    def __validate_read_one_region_path(self, path, target):
        return self._validate_readwrite_one_region_path(path, target, True)

    def __validate_write_one_region_path(self, path, target):
        return self._validate_readwrite_one_region_path(path, target, False)

    def _validate_region(self, target):
        path = getattr(self.Meta, 'region_path_for_read_permissions', None)
        if path:
            if isinstance(path, (list, tuple)):
                for onepath in path:
                    self.__validate_read_one_region_path(onepath, target)
            else:
                self.__validate_read_one_region_path(path, target)
        path = getattr(self.Meta, 'region_path_for_write_permissions', None)
        if path:
            if isinstance(path, (list, tuple)):
                for onepath in path:
                    self.__validate_write_one_region_path(onepath, target)
            else:
                self.__validate_write_one_region_path(path, target)

    def validate(self, attrs):
        data = super().validate(attrs)
        self._validate_region(data)
        return data

    def update(self, instance, validated_data):
        # Comprobamos que el objeto que se pretende actualizar existe
        # en una región permitida
        self._validate_region(instance)
        return super().update(instance, validated_data)


class MiddleRegionSerializer(serializers.ModelSerializer):
    parent_object = serializers.SerializerMethodField()

    class Meta:
        model = models.Region
        fields = (
            'pk',
            'name',
            'parent',
            'parent_object',
            'level',
            'postal_code',
            'geom',
        )
        read_only_fields = (
            'pk',
            'name',
            'parent',
            'parent_object',
            'level',
            'postal_code',
            'geom',
        )

    def get_parent_object(self, obj):
        serializer = MiddleRegionSerializer(instance=obj.parent, many=False)
        return serializer.data


class MiddlePersonSerializer(serializers.ModelSerializer):
    region_object = MiddleRegionSerializer(read_only=True, source='region')

    class Meta:
        model = models.Person
        fields = (
            'pk',
            'role',
            'name',
            'contact_person_name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'phone',
            'point',
            'production_capacity',
            'logistics_need',
            'user',
            'can_make_orders',
            'number',
            'city',
            'province',
            'cp',
            'country',
            'role_disambiguation',
        )
        read_only_fields = (
            'pk',
            'role',
            'region_object',
            'can_make_orders',
            'user',
        )

    def create(self, validated_data):
        # forzamos que este serializer sólo registre makers
        validated_data.update({
            'role': models.Person.ROLE_LOGISTICS,
            'user': self.context.get('request').user,
        })
        return super().create(validated_data)

    def update(self, instance, validated_data):
        # Security check. Un role no puede cambiarse para evitar
        # escalada de privilegios
        if 'role' in validated_data:
            del validated_data['role']
        return super().update(instance, validated_data)


class MiddleRoleDisambiguationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.RoleDisambiguation
        fields = (
            'pk',
            'role',
            'name',
        )


class MiddleForeignPersonSerializer(HandleRegionPermissions,
                                    serializers.ModelSerializer):
    region_object = MiddleRegionSerializer(read_only=True, source='region')
    role_disambiguation_object = MiddleRoleDisambiguationSerializer(
        read_only=True,
        source='role_disambiguation',
    )

    class Meta:
        model = models.Person
        region_path_for_write_permissions = 'region'
        fields = (
            'pk',
            'role',
            'role_disambiguation',
            'role_disambiguation_object',
            'name',
            'contact_person_name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'phone',
            'point',
            'production_capacity',
            'logistics_need',
            'user',
            'can_make_orders',
            'number',
            'city',
            'province',
            'cp',
            'country',
        )
        read_only_fields = (
            'pk',
            'region_object',
            'role_disambiguation_object',
            'user',
        )

    def create(self, validated_data):
        if validated_data.get('role') == models.Person.ROLE_LOGISTICS:
            raise serializers.ValidationError(
                {
                    'role': 'Role type set denied'
                },
                code='denied',
            )
        return super().create(validated_data)

    def update(self, instance, validated_data):
        # Security check. Un role no puede cambiarse para evitar
        # escalada de privilegios
        if 'role' in validated_data:
            del validated_data['role']
        return super().update(instance, validated_data)


class MiddleCarrierSerializer(HandleRegionPermissions,
                              serializers.ModelSerializer):
    region_object = MiddleRegionSerializer(read_only=True, source='region')

    class Meta:
        model = models.Person
        region_path_for_write_permissions = 'region'
        fields = (
            'pk',
            'role',
            'name',
            'contact_person_name',
            'region',
            'region_object',
            'telegram_nick',
            'telegram_id',
            'address',
            'phone',
            'point',
            'production_capacity',
            'user',
            'number',
            'city',
            'province',
            'cp',
            'country',
        )
        read_only_fields = (
            'pk',
            'role',
            'region_object',
        )


class MiddleResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Resource
        fields = (
            'pk',
            'resource_type',
            'name',
            'sku',
            'spl_link',
            'image_link',
        )


class MiddleRequestShippingInventorySerializer(serializers.ModelSerializer):
    """Serializer anidado de inventory de sólo lectura
    """
    quantity_available = serializers.IntegerField(read_only=True)
    auto_gen_batch = serializers.IntegerField(read_only=True)
    pad_batch = serializers.CharField(read_only=True)
    owner_object = MiddlePersonSerializer(read_only=True, source='owner')
    resource_object = MiddleResourceSerializer(
        read_only=True,
        source='resource',
    )

    class Meta:
        model = models.Inventory
        fields = (
            'pk',
            'resource',
            'resource_object',
            'owner',
            'owner_object',
            'quantity',
            'track_batch',
            'photo_link',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class MiddleNestedRequestShippingSerializer(serializers.ModelSerializer):
    """Serializer anidado de shipping de sólo lectura
    """
    inventory_object = MiddleRequestShippingInventorySerializer(
        read_only=True,
        source='inventory',
    )
    carrier_object = MiddleCarrierSerializer(read_only=True, source='carrier')

    class Meta:
        model = models.Shipping
        fields = (
            'pk',
            'uuid',
            'status',
            'inventory',
            'inventory_object',
            'carrier',
            'carrier_object',
            'carrier_track_num',
            'carrier_label_base64',
            'carrier_label_base64_format',
            'request',
            'quantity',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class MiddleRequestSerializer(HandleRegionPermissions,
                              serializers.ModelSerializer):
    resource_object = MiddleResourceSerializer(
        read_only=True,
        source='resource',
    )
    consumer_object = MiddlePersonSerializer(read_only=True, source='consumer')
    shippings_object = MiddleNestedRequestShippingSerializer(
        read_only=True,
        many=True,
        source='shippings',
    )
    quantity_satisfied = serializers.SerializerMethodField()

    class Meta:
        model = models.Request
        region_path_for_write_permissions = 'consumer__region'
        fields = (
            'pk',
            'status',
            'consumer',
            'consumer_object',
            'resource',
            'resource_object',
            'quantity',
            'done',
            'shippings_object',
            'quantity_satisfied',
        )
        read_only_fields = (
            'pk',
            'resource_object',
            'consumer_object',
            'shippings_object',
        )

    def get_quantity_satisfied(self, obj):
        if getattr(obj, 'quantity_satisfied', None):
            return obj.quantity_satisfied
        return None


class MiddleInventorySerializer(HandleRegionPermissions,
                                serializers.ModelSerializer):
    quantity_available = serializers.IntegerField(read_only=True)
    auto_gen_batch = serializers.IntegerField(read_only=True)
    pad_batch = serializers.CharField(read_only=True)
    owner_object = MiddlePersonSerializer(read_only=True, source='owner')
    resource_object = MiddleResourceSerializer(
        read_only=True,
        source='resource',
    )
    distance = serializers.SerializerMethodField()
    sum_movements_to_this = serializers.SerializerMethodField()
    sum_request_consumed = serializers.SerializerMethodField()
    sum_awaiting_movements_to_this = serializers.SerializerMethodField()
    sum_awaiting_request_consumed = serializers.SerializerMethodField()

    quantity_available = serializers.SerializerMethodField()

    class Meta:
        model = models.Inventory
        region_path_for_write_permissions = 'owner__region'
        fields = (
            'pk',
            'resource',
            'resource_object',
            'owner',
            'owner_object',
            'quantity',
            'track_batch',
            'photo_link',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
            'distance',
            'sum_movements_to_this',
            'sum_request_consumed',
            'sum_awaiting_movements_to_this',
            'sum_awaiting_request_consumed',
        )
        read_only_fields = (
            'pk',
            # 'resource',
            'resource_object',
            # 'owner',
            'owner_object',
            # 'quantity',
            'track_batch',
            # 'photo_link',
            'auto_gen_batch',
            'pad_batch',
            'quantity_available',
        )

    def get_distance(self, obj):
        if getattr(obj, 'distance', None):
            return obj.distance.m
        return None

    def get_sum_movements_to_this(self, obj):
        if getattr(obj, 'sum_movements_to_this', None):
            return obj.sum_movements_to_this
        return 0

    def get_sum_request_consumed(self, obj):
        if getattr(obj, 'sum_request_consumed', None):
            return obj.sum_request_consumed
        return 0

    def get_sum_awaiting_movements_to_this(self, obj):
        if getattr(obj, 'sum_awaiting_movements_to_this', None):
            return obj.sum_awaiting_movements_to_this
        return None

    def get_sum_awaiting_request_consumed(self, obj):
        if getattr(obj, 'sum_awaiting_request_consumed', None):
            return obj.sum_awaiting_request_consumed
        return None

    def get_quantity_available(self, obj):
        if getattr(obj, 'sum_quantity_available', None):
            return obj.sum_quantity_available
        return obj.quantity_available


class MiddleShippingRequestSerializer(serializers.ModelSerializer):
    """Serializer de request anidado de sólo lectura
    """
    resource_object = MiddleResourceSerializer(
        read_only=True,
        source='resource',
    )
    consumer_object = MiddlePersonSerializer(read_only=True, source='consumer')

    class Meta:
        model = models.Request
        fields = (
            'pk',
            'status',
            'consumer',
            'consumer_object',
            'resource',
            'resource_object',
            'quantity',
            'done',
        )
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class MiddleShippingSerializer(serializers.ModelSerializer):
    """Serializer de Shipping básico
    """
    inventory_object = MiddleInventorySerializer(
        read_only=True,
        source='inventory',
    )
    carrier_object = MiddleCarrierSerializer(read_only=True, source='carrier')
    request_object = MiddleShippingRequestSerializer(
        read_only=True,
        source='request',
    )
    move_object = MiddleInventorySerializer(
        read_only=True,
        source='move',
    )

    class Meta:
        model = models.Shipping
        fields = (
            'pk',
            'uuid',
            'status',
            'inventory',
            'inventory_object',
            'carrier',
            'carrier_object',
            'carrier_track_num',
            'carrier_label_base64',
            'carrier_label_base64_format',
            'request',
            'request_object',
            'quantity',
            'move',
            'move_object',
            'created_date',
        )
        read_only_fields = (
            'pk',
            'uuid',
            'inventory_object',
            'request_object',
            'carrier_object',
            'move_object',
        )

    def validate(self, attrs):
        if self.context.get('request').method == 'POST':
            # Evitar que un usuario cree envíos en pedidos en regiones en las
            # que no tiene permitido hacerlo. El resto de methods se testean
            # en el view.
            request = attrs.get('request', None)
            move = attrs.get('move', None)

            if move and request:
                raise serializers.ValidationError(
                    {
                        'request': 'Only one of both',
                        'move': 'Only one of both'
                    },
                    code='denied',
                )

            if not move and not request:
                raise serializers.ValidationError(
                    {
                        'request': 'Need one',
                        'move': 'Need one'
                    },
                    code='denied',
                )

            if request:
                person = self.context.get('view').current_person
                region = attrs.get('request').consumer.region
                if not person.logistica_can_read_region(region):
                    raise serializers.ValidationError(
                        {
                            'request': 'Permission denied'
                        },
                        code='denied',
                    )

            if move:
                person = self.context.get('view').current_person
                region = attrs.get('move').owner.region
                if not person.logistica_can_read_region(region):
                    raise serializers.ValidationError(
                        {
                            'move': 'Permission denied'
                        },
                        code='denied',
                    )

        return super().validate(attrs)


class MiddleRequestShippingSerializer(MiddleShippingSerializer):
    """Mismo serializer que MiddleShippingSerializer, pero pensado
    para endpoints donde el request_pk venga en la url
    """

    class Meta(MiddleShippingSerializer.Meta):
        read_only_fields = MiddleShippingSerializer.Meta.read_only_fields + (
            'request', )

    def validate(self, attrs):
        request_pk = self.context.get('view').kwargs.get('request_pk')
        if not request_pk:
            raise serializers.ValidationError(
                {
                    'request': 'Permission denied'
                },
                code='denied',
            )
        if request_pk:
            # llegamos hasta aquí a través de un endpoint donde se indica
            # el shipping_pk y el request_pk en la url. Obtenemos el request
            # y lo seteamos.
            # Cuando es la creación de un objeto nuevo, se validará en el
            # validate padre super().validate. Cuando es edición de objeto ya
            # existente, en el queryset del classview se comprueba que tenga
            # acceso y que el request corresponda al shipping.
            try:
                request = models.Request.objects.get(
                    pk=self.context.get('view').kwargs.get('request_pk'))
                attrs['request'] = request
            except models.Request.DoesNotExist:
                raise serializers.ValidationError(
                    {
                        'request': 'Permission denied'
                    },
                    code='denied',
                )
        return super().validate(attrs)


class MiddleAutoMovementConfigurationResourceBulkSerializer(
        HandleRegionPermissions, serializers.ModelSerializer):
    resource_bulk = serializers.ListField(
        required=False,
        child=serializers.IntegerField(),
    )
    person = serializers.IntegerField(required=False, )

    target_inventory_bulk = MiddleInventorySerializer(
        read_only=True, many=True)

    class Meta:
        model = models.AutoMovementConfiguration
        region_path_for_write_permissions = (
            'source_region',
            'target_inventory__owner__region',
        )
        fields = [
            'person',
            'source_region',
            'resource_bulk',
            'carrier',
            'target_inventory',
            'target_inventory_bulk',
        ]

    def validate(self, attrs):
        data = super().validate(attrs)
        if data.get('resource') and data.get('resource_bulk'):
            raise serializers.ValidationError(
                {
                    'resource': 'Only one of both',
                    'resource_bulk': 'Only one of both'
                },
                code='unique',
            )

        if data.get('resource_bulk') and not data.get('person'):
            raise serializers.ValidationError(
                {
                    'person': 'Fill persot to use resource_bulk',
                },
                code='required',
            )

        if data.get('person') and not data.get('resource_bulk'):
            raise serializers.ValidationError(
                {
                    'resource_bulk': 'Fill resource_builk with person',
                },
                code='required',
            )

        if data.get('resource_bulk') and data.get('target_inventory'):
            raise serializers.ValidationError(
                {
                    'target_inventory': 'Skip usage with resource_bulk',
                },
                code='required',
            )
        return data

    def create(self, validated_data):
        resource_bulk = validated_data.get('resource_bulk')
        if resource_bulk:
            person_pk = validated_data.get('person')
            person = models.Person.objects.get(pk=person_pk)
            source_region = models.Region.objects.get(
                pk=validated_data.get('source_region').pk)
            carrier = models.Person.objects.get(
                pk=validated_data.get('carrier').pk)
            output_data = {'target_inventory_bulk': [], 'resource_bulk': []}
            for resource_pk in resource_bulk:
                resource = models.Resource.objects.get(pk=resource_pk)
                inventory, _ = models.Inventory.objects.get_or_create(
                    owner=person,
                    resource=resource,
                )
                new_data = {
                    'source_region': source_region.pk,
                    'resource': resource.pk,
                    'carrier': carrier.pk,
                    'target_inventory': inventory.pk,
                }
                _ser = MiddleAutoMovementConfigurationSubSerializer(
                    data=new_data, context=self.context)
                _ser.is_valid(raise_exception=True)
                _ser.save()
                _out_data = _ser.data
                output_data['target_inventory_bulk'].append(inventory.pk)
                output_data['resource_bulk'].append(resource.pk)
                output_data.update(new_data)
            return output_data
        return super().create(validated_data)


class MiddleAutoMovementConfigurationSubSerializer(
        MiddleAutoMovementConfigurationResourceBulkSerializer):
    class Meta:
        model = models.AutoMovementConfiguration
        region_path_for_write_permissions = (
            'source_region',
            'target_inventory__owner__region',
        )
        fields = [
            'source_region',
            'resource',
            'carrier',
            'target_inventory',
            'target_inventory_bulk',
        ]


class MiddleAutoMovementConfigurationBulkSerializer(serializers.Serializer):
    bulk = serializers.ListField(
        required=True,
        child=serializers.DictField(),
    )

    class Meta:
        fields = ('bulk', )

    def create(self, validated_data):
        data = validated_data
        bulk = data.get('bulk')
        output_data = list()
        with transaction.atomic():
            for one_bulk in bulk:
                _ser = MiddleAutoMovementConfigurationResourceBulkSerializer(
                    data=one_bulk, context=self.context)
                _ser.is_valid(raise_exception=True)
                # calls serializer create
                object_instances = _ser.save()
                output_data.append(object_instances)

            return {'bulk': output_data}

    def update(self, instance, validated_data):
        return None


class MiddleAutoMovementConfigurationSerializer(serializers.ModelSerializer):
    source_region_object = MiddleRegionSerializer(
        read_only=True,
        source='source_region',
    )
    carrier_object = MiddleCarrierSerializer(read_only=True, source='carrier')
    target_inventory_object = MiddleInventorySerializer(
        read_only=True,
        source='target_inventory',
    )
    resource_object = MiddleResourceSerializer(
        read_only=True,
        source='resource',
    )

    class Meta:
        model = models.AutoMovementConfiguration
        region_path_for_write_permissions = (
            'source_region',
            'target_inventory__owner__region',
        )
        fields = [
            'pk',
            'source_region',
            'source_region_object',
            'resource',
            'resource_object',
            'carrier',
            'carrier_object',
            'target_inventory',
            'target_inventory_object',
        ]
