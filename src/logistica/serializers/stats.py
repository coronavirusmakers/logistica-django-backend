from rest_framework import serializers
from logistica import models


class StatsRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Region
        fields = (
            'pk',
            'name',
        )
        read_only_fields = (
            'pk',
            'name',
        )


class StatsResourceSerializer(serializers.ModelSerializer):
    total = serializers.IntegerField()

    class Meta:
        model = models.Resource
        fields = (
            'pk',
            'resource_type',
            'name',
            'sku',
            'spl_link',
            'image_link',
            'volume',
            'volume_unit',
            'total',
        )


class StatsByDayResourcesStatsSerializer(serializers.Serializer):
    build_static_labels = serializers.ListField()
    build_calculated_labels = serializers.ListField()
    deliver_static_labels = serializers.ListField()
    deliver_calculated_labels = serializers.ListField()

    build_static_values = serializers.ListField()
    build_calculated_values = serializers.ListField()
    deliver_static_values = serializers.ListField()
    deliver_calculated_values = serializers.ListField()

    class Meta:
        fields = (
            'build_static_labels',
            'build_calculated_labels',
            'deliver_static_labels',
            'deliver_calculated_labels',
            'build_static_values',
            'build_calculated_values',
            'deliver_static_values',
            'deliver_calculated_values',
        )

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class StatsByDayByRegionResourcesStatsSerializer(serializers.Serializer):
    region = StatsRegionSerializer()
    stats = StatsByDayResourcesStatsSerializer()

    class Meta:
        fields = {
            'region',
            'stats',
        }
        read_only_fields = fields

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class StatsByDayResourcesSerializer(serializers.ModelSerializer):
    stats_by_region = serializers.SerializerMethodField()

    class Meta:
        model = models.Resource
        fields = (
            'pk',
            'resource_type',
            'name',
            'sku',
            'spl_link',
            'image_link',
            'volume',
            'volume_unit',
            'stats_by_region',
        )

    def get_stats_by_region(self, obj):

        by_region = dict()

        stats = self.context.get('stats_queryset').filter(
            resource=obj).order_by('date')
        for stat in stats:
            region_pk = stat.region.pk
            if stat.region.pk not in by_region:
                by_region[region_pk] = {
                    'region': stat.region,
                    'stats': {
                        'build_static_labels': list(),
                        'build_calculated_labels': list(),
                        'deliver_static_labels': list(),
                        'deliver_calculated_labels': list(),
                        'build_static_values': list(),
                        'build_calculated_values': list(),
                        'deliver_static_values': list(),
                        'deliver_calculated_values': list(),
                    }
                }
            by_region[region_pk]['stats']['build_static_labels'].append(
                str(stat.date))
            by_region[stat.region.
                      pk]['stats']['build_calculated_labels'].append(
                          str(stat.date))
            by_region[region_pk]['stats']['deliver_static_labels'].append(
                str(stat.date))
            by_region[stat.region.
                      pk]['stats']['deliver_calculated_labels'].append(
                          str(stat.date))
            by_region[region_pk]['stats']['build_static_values'].append(
                stat.build_static_value)
            by_region[stat.region.
                      pk]['stats']['build_calculated_values'].append(
                          stat.build_calculated_value)
            by_region[region_pk]['stats']['deliver_static_values'].append(
                stat.deliver_static_value)
            by_region[stat.region.
                      pk]['stats']['deliver_calculated_values'].append(
                          stat.deliver_calculated_value)

        # to_serialize = {
        #     'build_static_labels': build_static_labels,
        #     'build_calculated_labels': build_calculated_labels,
        #     'deliver_static_labels': deliver_static_labels,
        #     'deliver_calculated_labels': deliver_calculated_labels,
        #     'build_static_values': build_static_values,
        #     'build_calculated_values': build_calculated_values,
        #     'deliver_static_values': deliver_static_values,
        #     'deliver_calculated_values': deliver_calculated_values,
        # }
        return StatsByDayByRegionResourcesStatsSerializer(
            by_region.values(), many=True).data


class StatsByDaySumResourcesStatsSerializer(serializers.Serializer):
    build_static_value = serializers.IntegerField()
    build_calculated_value = serializers.IntegerField()
    deliver_static_value = serializers.IntegerField()
    deliver_calculated_value = serializers.IntegerField()

    class Meta:
        fields = (
            'build_static_values',
            'build_calculated_values',
            'deliver_static_values',
            'deliver_calculated_values',
        )

    def create(self, validated_data):
        return None

    def update(self, instance, validated_data):
        return None


class StatsByDaySumResourcesSerializer(serializers.ModelSerializer):
    stats_sum = serializers.SerializerMethodField()

    class Meta:
        model = models.Resource
        fields = (
            'pk',
            'resource_type',
            'name',
            'sku',
            'spl_link',
            'image_link',
            'volume',
            'volume_unit',
            'stats_sum',
        )

    def get_stats_sum(self, obj):
        return StatsByDaySumResourcesStatsSerializer({
            'build_static_value':
            obj.build_static_value,
            'build_calculated_value':
            obj.build_calculated_value,
            'deliver_static_value':
            obj.deliver_static_value,
            'deliver_calculated_value':
            obj.deliver_calculated_value,
        }).data
