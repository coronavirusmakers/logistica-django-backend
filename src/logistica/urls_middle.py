from django.conf.urls import url
from .classviews import middle as middleviews

urlpatterns = [
    # Endpoints básicos, accesibles por un usuario normal cuando
    # se registra
    # Persons de cualqueir tipo
    # Endpoints logística
    #
    # Persons de tipo logistica
    url(
        r'^person/$',
        middleviews.MiddlePersonLC.as_view(),
        name='middle_person_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/$',
        middleviews.MiddlePersonRUD.as_view(),
        name='middle_person_rud',
    ),
    # Pedidos a los que tiene acceso la persona logística
    url(
        r'^person/(?P<person_pk>\d+)/request/$',
        middleviews.MiddleRequestLC.as_view(),
        name='middle_person_request_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/request/(?P<request_pk>\d+)/$',
        middleviews.MiddleRequestRUD.as_view(),
        name='middle_person_request_rud',
    ),
    # Shipping a través de pk de pedido
    url(
        r'^person/(?P<person_pk>\d+)/request/(?P<request_pk>\d+)/shipping/$',
        middleviews.MiddleRequestShippingLC.as_view(),
        name='middle_person_request_shipping_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/request/(?P<request_pk>\d+)/shipping/(?P<shipping_pk>\d+)/$',
        middleviews.MiddleRequestShippingRUD.as_view(),
        name='middle_person_request_shipping_rud',
    ),
    # Regiones a las que tiene acceso la persona logística
    url(
        r'^person/(?P<person_pk>\d+)/region/$',
        middleviews.MiddleRegionL.as_view(),
        name='middle_person_region_lc',
    ),
    # Estados de pedido disponibles
    url(
        r'^person/(?P<person_pk>\d+)/requeststatus/$',
        middleviews.MiddleRequestStatusL.as_view(),
        name='middle_person_requeststatus',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/shippingstatus/$',
        middleviews.MiddleShippingStatusL.as_view(),
        name='middle_person_shippingstatus',
    ),
    # Inventario disponible
    url(
        r'^person/(?P<person_pk>\d+)/inventory/$',
        middleviews.MiddleInventoryLC.as_view(),
        name='middle_person_inventory_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/inventory/(?P<inventory_pk>\d+)/$',
        middleviews.MiddleInventoryRUD.as_view(),
        name='middle_person_inventory_rud',
    ),
    # Shipping
    url(
        r'^person/(?P<person_pk>\d+)/shipping/$',
        middleviews.MiddleShippingLC.as_view(),
        name='middle_person_shipping_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/shipping/(?P<shipping_pk>\d+)/$',
        middleviews.MiddleShippingRUD.as_view(),
        name='middle_person_shipping_rud',
    ),
    # Pedidos a los que tiene acceso la persona logística
    url(
        r'^person/(?P<person_pk>\d+)/foreignperson/$',
        middleviews.MiddleForeignPersonLC.as_view(),
        name='middle_person_foreignperson_lc',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/foreignperson/(?P<foreignperson_pk>\d+)/$',
        middleviews.MiddleForeignPersonRUD.as_view(),
        name='middle_person_foreignperson_rud',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/automovementconfig/bulk/$',
        middleviews.MiddleAutoMovementConfigurationBulkC.as_view(),
        name='middle_person_automovementconfig_c',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/automovementconfig/$',
        middleviews.MiddleAutoMovementConfigurationL.as_view(),
        name='middle_person_automovementconfig_l',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/automovementconfig/(?P<automovementconfig_pk>\d+)/$',
        middleviews.MiddleAutoMovementConfigurationRUD.as_view(),
        name='middle_person_automovementconfig_l',
    ),
]
