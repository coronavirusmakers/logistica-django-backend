# pylint: disable=too-many-locals,too-many-statements,too-many-lines
from django.apps import apps
from django.urls import reverse
from django.conf import settings
from django.http import QueryDict
from django.contrib.gis.geos import Point
from rest_framework.test import APITestCase
from logistica.models import (
    Region,
    Resource,
    Person,
    Request,
    Inventory,
    RoleDisambiguation,
    AutoMovementConfiguration,
    Shipping,
)


class BaseApiTestCase(APITestCase):
    count = 0

    def _create_new_user(self, username='usertest'):
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(
            username=username, email=str(self.count) + 'asdf@example.com')
        self.count += 1
        return user

    def get_or_create_one_region(self):
        region, _ = Region.objects.get_or_create(name="region test", )
        return region

    def get_or_create_one_resource(self):
        resource, _ = Resource.objects.get_or_create(name="visera")
        return resource

    def get_new_logistica_person(self, user):
        logistica_person = Person.objects.create(
            user=user,
            role=Person.ROLE_LOGISTICS,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_LOGISTICS, name='Logistics'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return logistica_person

    def get_new_consumer_person(self, username="consumer"):
        consumer_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_CONSUMER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CONSUMER, name='Consumer'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return consumer_person

    def get_new_maker_person(self, username="maker"):
        maker_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_PRODUCER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_PRODUCER, name='Maker'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return maker_person

    def get_midpoint_person(self, username='midpoint'):
        maker_person = Person.objects.create(
            user=self._create_new_user(username),
            role=Person.ROLE_MIDPOINT,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_MIDPOINT, name='Midpoint'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return maker_person

    def get_new_carrier_person(self):
        carrier_person = Person.objects.create(
            user=self._create_new_user('carrier'),
            role=Person.ROLE_CARRIER,
            role_disambiguation=RoleDisambiguation.objects.create(
                role=Person.ROLE_CARRIER, name='Carrier'),
            region=self.get_or_create_one_region(),
            point=Point(27, 27),
            cp='00',
        )
        return carrier_person

    def _post(self, urlname=None, data=None, kwargs=None):
        return self.client.post(
            reverse(urlname, kwargs=kwargs),
            data,
            format='json',
        )

    def _patch(self, urlname=None, data=None, kwargs=None):
        return self.client.patch(
            reverse(urlname, kwargs=kwargs),
            data,
            format='json',
        )

    def _create(self, urlname=None, data=None, kwargs=None):
        return self._post(urlname, data, kwargs)

    def _list(self, urlname=None, filters=None, kwargs=None):
        if filters is None:
            return self.client.get(
                reverse(urlname, kwargs=kwargs),
                format='json',
            )
        _q = QueryDict('', mutable=True)
        _q.update(filters)
        return self.client.get(
            '%s?%s' % (reverse(urlname, kwargs=kwargs), _q.urlencode()),
            format='json',
        )

    def _read(self, urlname=None, kwargs=None):
        return self.client.get(
            reverse(urlname, kwargs=kwargs),
            format='json',
        )

    def login(self, user=None):
        if user is None:
            user = self._create_new_user()
        user.set_password('passtest')
        user.save()

        resp = self.client.post(
            reverse("jwt-create"),
            {
                'username': user.username,
                'password': 'passtest',
            },
            format='json',
        )
        token = resp.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return user, resp

    def logout(self):
        pass


class LogisticaTestCase(BaseApiTestCase):
    def test_person_create_list(self):
        user, _ = self.login()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_LOGISTICS, name='Logistica')

        # Person logistica creation
        resp = self.client.post(
            reverse("middle_person_lc"),
            {
                'name': 'Person test',
                'region': self.get_or_create_one_region().pk,
                'cp': '00',
                'role_disambiguation': role_disambiguation.pk,
                'point': {
                    'type': 'Point',
                    'coordinates': [27, 27]
                },
            },
            format='json',
        )
        self.assertEqual(int(resp.data['user']), int(user.pk))
        self.assertEqual(int(resp.data['role']), int(Person.ROLE_LOGISTICS))
        person_pk = resp.data['pk']

        # person list
        resp = self.client.get(
            reverse("middle_person_lc"),
            format='json',
        )
        self.assertEqual(resp.data.get('results')[0].get('pk'), person_pk)

        # person isolation (security)
        cls = apps.get_model(settings.AUTH_USER_MODEL)
        user = cls.objects.create(username="usertest2")
        self.login(user)
        resp = self.client.get(
            reverse("middle_person_lc"),
            format='json',
        )
        self.assertEqual(resp.data['count'], 0)

    def test_midpoint_person_create(self):
        user, _ = self.login()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')
        region = self.get_or_create_one_region()
        logistics_person = self.get_new_logistica_person(user)

        # Person logistica creation without permisisons
        resp = self.client.post(
            reverse(
                "middle_person_foreignperson_lc",
                kwargs={'person_pk': logistics_person.pk},
            ),
            {
                'name': 'Person middpoint',
                'region': region.pk,
                'cp': '00',
                'role_disambiguation': role_disambiguation.pk,
                'role': Person.ROLE_MIDPOINT,
                'point': {
                    'type': 'Point',
                    'coordinates': [27, 27]
                },
            },
            format='json',
        )
        # cannot create, permission denied
        self.assertEqual(resp.status_code, 400)

        # grant permissions
        logistics_person.logistica_region_write.add(region)
        resp = self.client.post(
            reverse(
                "middle_person_foreignperson_lc",
                kwargs={'person_pk': logistics_person.pk},
            ),
            {
                'name': 'Person middpoint',
                'region': region.pk,
                'cp': '00',
                'role_disambiguation': role_disambiguation.pk,
                'role': Person.ROLE_MIDPOINT,
                'point': {
                    'type': 'Point',
                    'coordinates': [27, 27]
                },
            },
            format='json',
        )

        # right role creation
        self.assertEqual(int(resp.data['role']), int(Person.ROLE_MIDPOINT))
        # right role disambiguation creation
        self.assertEqual(
            int(resp.data['role_disambiguation']), int(role_disambiguation.pk))
        # right raw object returned
        self.assertEqual(
            int(resp.data['role_disambiguation_object']['role']),
            int(RoleDisambiguation.ROLE_MIDPOINT))

        foreign_pk = resp.data.get('pk')

        # revoke permissions
        logistics_person.logistica_region_write.remove(region)
        # can patch?
        resp = self.client.patch(
            reverse(
                "middle_person_foreignperson_rud",
                kwargs={
                    'person_pk': logistics_person.pk,
                    'foreignperson_pk': foreign_pk,
                },
            ),
            {
                'name': 'Person middpoint2',
            },
            format='json',
        )
        self.assertEqual(resp.status_code, 404)

    def test_bad_person_create(self):
        user, _ = self.login()
        role_disambiguation = RoleDisambiguation.objects.create(
            role=RoleDisambiguation.ROLE_LOGISTICS, name='midpoint')
        region = self.get_or_create_one_region()
        logistics_person = self.get_new_logistica_person(user)

        # Person logistica creation
        resp = self.client.post(
            reverse(
                "middle_person_foreignperson_lc",
                kwargs={'person_pk': logistics_person.pk},
            ),
            {
                'name': 'Person middpoint',
                'region': region.pk,
                'cp': '00',
                'role_disambiguation': role_disambiguation.pk,
                'role': Person.ROLE_LOGISTICS,
                'point': {
                    'type': 'Point',
                    'coordinates': [27, 27]
                },
            },
            format='json',
        )
        # disallow logistics person creation
        self.assertEqual(resp.status_code, 400)

    def test_request_access(self):
        user, _ = self.login()
        resource = self.get_or_create_one_resource()
        logistica_person = self.get_new_logistica_person(user)
        allowed_region = Region.objects.create(name="region test 1")
        denied_region = Region.objects.create(name="region test 2")
        logistica_person.logistica_region_write.add(allowed_region)
        logistica_person.save()
        consumer = self.get_new_consumer_person()
        consumer.region = allowed_region
        consumer.save()

        # allowed Request creation
        resp = self.client.post(
            reverse(
                "middle_person_request_lc",
                kwargs={'person_pk': logistica_person.pk},
            ),
            {
                'consumer': consumer.pk,
                'resource': resource.pk,
                'quantity': 12,
                'region': self.get_or_create_one_region().pk,
            },
            format='json',
        )
        self.assertEqual(resp.status_code, 201)
        request_pk = resp.data.get('pk')

        # allowed request list
        resp = self.client.get(
            reverse(
                "middle_person_request_lc",
                kwargs={'person_pk': logistica_person.pk}),
            format='json',
        )
        self.assertEqual(resp.data.get('results')[0].get('pk'), request_pk)

        # allowed request read:
        resp = self.client.get(
            reverse(
                "middle_person_request_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                },
            ),
            format='json',
        )
        self.assertEqual(resp.data.get('pk'), request_pk)

        # allowed request patch:
        resp = self.client.patch(
            reverse(
                "middle_person_request_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                },
            ),
            {'status': Request.STATUS_RECEIVED},
            format='json',
        )
        self.assertEqual(resp.data.get('pk'), request_pk)
        self.assertEqual(resp.data.get('status'), Request.STATUS_RECEIVED)

        # change consumer region test denied access
        consumer.region = denied_region
        consumer.save()

        # denied request list
        resp = self.client.get(
            reverse(
                "middle_person_request_lc",
                kwargs={'person_pk': logistica_person.pk}),
            format='json',
        )
        self.assertEqual(resp.data.get('count'), 0)

        # denied request read
        resp = self.client.get(
            reverse(
                "middle_person_request_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                }),
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # denied request patch:
        resp = self.client.patch(
            reverse(
                "middle_person_request_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                },
            ),
            {'status': Request.STATUS_RECEIVED},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

    def test_request__shipping(self):
        user, _ = self.login()
        resource = self.get_or_create_one_resource()
        logistica_person = self.get_new_logistica_person(user)
        allowed_region = Region.objects.create(name="region test 1")
        denied_region = Region.objects.create(name="region test 2")
        logistica_person.logistica_region_write.add(allowed_region)
        logistica_person.save()
        consumer = self.get_new_consumer_person()
        consumer.region = allowed_region
        consumer.save()
        carrier = self.get_new_carrier_person()
        carrier.region = allowed_region
        inventory = Inventory.objects.create(
            owner=self.get_new_maker_person(),
            resource=resource,
            quantity=123,
        )

        # allowed Request creation
        resp = self.client.post(
            reverse(
                "middle_person_request_lc",
                kwargs={'person_pk': logistica_person.pk},
            ),
            {
                'consumer': consumer.pk,
                'resource': resource.pk,
                'quantity': 12,
                'region': self.get_or_create_one_region().pk,
            },
            format='json',
        )
        self.assertEqual(resp.status_code, 201)
        request_pk = resp.data.get('pk')

        # allowed Shipping creation
        resp = self.client.post(
            reverse(
                "middle_person_request_shipping_lc",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                },
            ),
            {
                'carrier': carrier.pk,
                'inventory': inventory.pk,
                'quantity': 12,
            },
            format='json',
        )
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.data.get('request'), request_pk)

        shipping_pk = resp.data.get('pk')

        # allowed shipping list
        resp = self.client.get(
            reverse(
                "middle_person_request_shipping_lc",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                }),
            format='json',
        )
        self.assertEqual(resp.data.get('results')[0].get('pk'), shipping_pk)

        # allowed shipping read:
        resp = self.client.get(
            reverse(
                "middle_person_request_shipping_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk,
                    'shipping_pk': shipping_pk,
                },
            ),
            format='json',
        )
        self.assertEqual(resp.data.get('pk'), shipping_pk)

        # allowed request patch:
        resp = self.client.patch(
            reverse(
                "middle_person_request_shipping_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk,
                    'shipping_pk': shipping_pk,
                },
            ),
            {'quantity': 111},
            format='json',
        )
        self.assertEqual(resp.data.get('pk'), shipping_pk)
        self.assertEqual(resp.data.get('quantity'), 111)

        # change consumer region test denied access
        consumer.region = denied_region
        consumer.save()

        # denied shipping list
        resp = self.client.get(
            reverse(
                "middle_person_request_shipping_lc",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk
                }),
            format='json',
        )
        self.assertEqual(resp.data.get('count'), 0)

        # denied shipping read
        resp = self.client.get(
            reverse(
                "middle_person_request_shipping_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk,
                    'shipping_pk': shipping_pk,
                }),
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

        # denied request patch:
        resp = self.client.patch(
            reverse(
                "middle_person_request_shipping_rud",
                kwargs={
                    'person_pk': logistica_person.pk,
                    'request_pk': request_pk,
                    'shipping_pk': shipping_pk,
                },
            ),
            {'quantity': 222},
            format='json',
        )
        self.assertEqual(resp.status_code, 403)

    def test_inventory_sum_quantity_list(self):
        user, _ = self.login()
        # role_disambiguation = RoleDisambiguation.objects.create(
        #     role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')

        region = self.get_or_create_one_region()
        logistics_person = self.get_new_logistica_person(user)
        # give permissions
        logistics_person.logistica_region_write.add(region)
        midpoint = self.get_midpoint_person()
        midpoint.region = region
        midpoint.save()
        maker = self.get_new_maker_person()
        maker.region = region
        maker.save()
        consumer = self.get_new_consumer_person()
        consumer.region = region
        consumer.save()
        resource = self.get_or_create_one_resource()

        midpoint_inventory = Inventory.objects.create(
            resource=resource,
            owner=midpoint,
            quantity=100,
        )
        Inventory.objects.create(
            resource=resource,
            owner=maker,
            quantity=100,
        )

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )

        # two inventories
        self.assertEqual(resp.data.get('count'), 2)
        self.assertEqual(
            resp.data.get('results')[0].get('quantity_available'), 100)
        self.assertEqual(
            resp.data.get('results')[1].get('quantity_available'), 100)

        # create auto movement to midpoint
        AutoMovementConfiguration.objects.create(
            source_region=region,
            resource=resource,
            carrier=None,
            target_inventory=midpoint_inventory)

        # Create new maker inventory to get automovement behavior
        moved_inventory = Inventory.objects.create(
            resource=resource,
            owner=maker,
            quantity=100,
        )

        shipping_for_move = moved_inventory.shippings.first()
        shipping_for_move.status = Shipping.STATUS_SENT
        shipping_for_move.save()

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )

        self.assertEqual(resp.data.get('count'), 2)
        # the second inventory of maker has been moved to midpoint
        self.assertEqual(
            resp.data.get('results')[0].get('quantity_available'), 100)
        self.assertEqual(
            resp.data.get('results')[1].get('quantity_available'), 200)

        # create request
        request = Request.objects.create(
            resource=resource,
            quantity=300,
            consumer=consumer,
        )
        Shipping.objects.create(
            status=Shipping.STATUS_SENT,
            request=request,
            quantity=80,
            inventory=midpoint_inventory,
        )

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )

        # after request+shipping, the middlepoint now has 120
        self.assertEqual(
            resp.data.get('results')[0].get('quantity_available'), 100)
        self.assertEqual(
            resp.data.get('results')[1].get('quantity_available'), 120)

        # new shipping with the rest of the midpoint inventory
        Shipping.objects.create(
            status=Shipping.STATUS_SENT,
            request=request,
            quantity=120,
            inventory=midpoint_inventory,
        )

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )

        # midpoint inventory should be empty and ignored from this list
        self.assertEqual(resp.data.get('count'), 1)
        # first maker inventory (not auto moved) should be listed
        self.assertEqual(
            resp.data.get('results')[0].get('quantity_available'), 100)

    def test_inventory_region_range_filter(self):
        user, _ = self.login()
        # role_disambiguation = RoleDisambiguation.objects.create(
        #     role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')
        region = self.get_or_create_one_region()
        region2 = Region.objects.create(name="region test2")
        region3 = Region.objects.create(name="region test2")
        logistics_person = self.get_new_logistica_person(user)
        # give permissions
        logistics_person.logistica_region_write.add(region)
        logistics_person.logistica_region_write.add(region2)
        midpoint = self.get_midpoint_person()
        midpoint.region = region2
        midpoint.save()
        maker = self.get_new_maker_person()
        maker.region = region
        maker.save()
        maker2 = self.get_new_maker_person("maker2")
        maker2.region = region3
        maker2.save()
        consumer = self.get_new_consumer_person()
        consumer.region = region
        consumer.save()
        resource = self.get_or_create_one_resource()
        Inventory.objects.create(
            resource=resource,
            owner=midpoint,
            quantity=100,
        )
        Inventory.objects.create(
            resource=resource,
            owner=maker,
            quantity=100,
        )
        Inventory.objects.create(
            resource=resource,
            owner=maker2,
            quantity=100,
        )

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region__in': f"{region.pk},{region2.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 2)

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region': region.pk,
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 1)

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region': region2.pk,
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 1)

        # Will list 2 (there is 3) because logistics role do not has
        # region3 perms yet
        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region__in': f"{region.pk},{region2.pk},{region3.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 2)

        # Add perms to region3
        logistics_person.logistica_region_write.add(region3)
        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region__in': f"{region.pk},{region2.pk},{region3.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 3)

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region__in': f"{region.pk},{region2.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 2)

        resp = self._list(
            urlname='middle_person_inventory_lc',
            filters={
                'with_avail_quantity': True,
                'ordering': '-pk',
                'owner__region__in': f"{region.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 1)

    def test_request_region_range_filter(self):
        user, _ = self.login()
        # role_disambiguation = RoleDisambiguation.objects.create(
        #     role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')
        region1 = self.get_or_create_one_region()
        region2 = Region.objects.create(name="region test2")
        region3 = Region.objects.create(name="region test2")
        logistics_person = self.get_new_logistica_person(user)
        resource = self.get_or_create_one_resource()

        consumer1 = self.get_new_consumer_person('consumer1')
        consumer1.region = region1
        consumer1.save()

        consumer2 = self.get_new_consumer_person('consumer2')
        consumer2.region = region2
        consumer2.save()

        consumer3 = self.get_new_consumer_person('consumer3')
        consumer3.region = region3
        consumer3.save()

        Request.objects.create(
            resource=resource,
            consumer=consumer1,
            quantity=100,
        )

        Request.objects.create(
            resource=resource,
            consumer=consumer2,
            quantity=100,
        )

        Request.objects.create(
            resource=resource,
            consumer=consumer3,
            quantity=100,
        )

        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 0)

        logistics_person.logistica_region_write.add(region1)
        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 1)

        logistics_person.logistica_region_write.add(region2)
        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 2)

        logistics_person.logistica_region_write.add(region3)
        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 3)

        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
                'consumer__region__in': region1.pk,
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 1)

        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering': '-pk',
                'consumer__region__in': f"{region1.pk}, {region2.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 2)

        resp = self._list(
            urlname='middle_person_request_lc',
            filters={
                'ordering':
                '-pk',
                'consumer__region__in':
                f"{region1.pk}, {region2.pk}, {region3.pk}",
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 3)

    def test_shipping_perms(self):
        user, _ = self.login()
        # role_disambiguation = RoleDisambiguation.objects.create(
        #     role=RoleDisambiguation.ROLE_MIDPOINT, name='midpoint')
        region1 = self.get_or_create_one_region()
        region2 = Region.objects.create(name="region test2")
        Region.objects.create(name="region test2")

        maker1 = self.get_new_maker_person()
        maker1.region = region1
        maker1.save()
        maker2 = self.get_new_maker_person("maker2")
        maker2.region = region2
        maker2.save()
        consumer1 = self.get_new_consumer_person('consumer1')
        consumer1.region = region1
        consumer1.save()
        resource = self.get_or_create_one_resource()
        request1 = Request.objects.create(
            resource=resource,
            consumer=consumer1,
            quantity=100,
        )

        logistics_person = self.get_new_logistica_person(user)

        maker1_inventory = Inventory.objects.create(
            resource=resource,
            owner=maker1,
            quantity=100,
        )
        maker2_inventory = Inventory.objects.create(
            resource=resource,
            owner=maker2,
            quantity=100,
        )
        shipping = Shipping.objects.create(
            quantity=80,
            inventory=maker1_inventory,
            move=maker2_inventory,
        )

        resp = self._list(
            urlname='middle_person_shipping_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )
        self.assertEqual(resp.data.get('count'), 0)

        # Cannot read movement
        resp = self._read(
            urlname='middle_person_shipping_rud',
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping.pk
            },
        )
        self.assertEqual(resp.status_code, 403)

        # grant perms to region 1 but not to region 2
        logistics_person.logistica_region_write.add(region1)
        resp = self._read(
            urlname='middle_person_shipping_rud',
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping.pk
            },
        )
        self.assertEqual(resp.status_code, 403)

        # Grant perms to region 2 (region of move-inventory object) and test
        logistics_person.logistica_region_write.add(region2)
        resp = self._read(
            urlname='middle_person_shipping_rud',
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping.pk
            },
        )
        self.assertEqual(resp.status_code, 200)

        # revoke perms
        logistics_person.logistica_region_write.remove(region1)
        logistics_person.logistica_region_write.remove(region2)

        # convert shiping from movement to request shipping
        shipping.move = None
        shipping.request = request1
        shipping.save()

        resp = self._read(
            urlname='middle_person_shipping_rud',
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping.pk
            },
        )
        self.assertEqual(resp.status_code, 403)

        # grant perms
        logistics_person.logistica_region_write.add(region1)
        resp = self._read(
            urlname='middle_person_shipping_rud',
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping.pk
            },
        )
        self.assertEqual(resp.status_code, 200)

        resp = self._list(
            urlname='middle_person_shipping_lc',
            filters={
                'ordering': '-pk',
            },
            kwargs={'person_pk': logistics_person.pk},
        )

        # revoke perms
        logistics_person.logistica_region_write.remove(region1)
        logistics_person.logistica_region_write.remove(region2)

        # movement denied
        resp = self._create(
            urlname='middle_person_shipping_lc',
            data={
                'quantity': 80,
                'inventory': maker1_inventory.pk,
                'move': maker2_inventory.pk,
            },
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 400)

        # request shipping denied
        resp = self._create(
            urlname='middle_person_shipping_lc',
            data={
                'quantity': 80,
                'inventory': maker1_inventory.pk,
                'request': request1.pk,
            },
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 400)

        # grant region
        logistics_person.logistica_region_write.add(region1)
        logistics_person.logistica_region_write.add(region2)

        # movement denied
        resp = self._create(
            urlname='middle_person_shipping_lc',
            data={
                'quantity': 80,
                'inventory': maker1_inventory.pk,
                'move': maker2_inventory.pk,
            },
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 201)

        # request shipping denied
        resp = self._create(
            urlname='middle_person_shipping_lc',
            data={
                'quantity': 80,
                'inventory': maker1_inventory.pk,
                'request': request1.pk,
            },
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 201)
        shipping_pk = resp.data.get('pk')

        logistics_person.logistica_region_write.remove(region1)
        logistics_person.logistica_region_write.remove(region2)

        # patch shipping denied
        resp = self._patch(
            urlname='middle_person_shipping_rud',
            data={
                'quantity': 82,
            },
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping_pk,
            },
        )
        self.assertEqual(resp.status_code, 403)

        logistics_person.logistica_region_write.add(region1)
        logistics_person.logistica_region_write.add(region2)

        # patch shipping denied
        resp = self._patch(
            urlname='middle_person_shipping_rud',
            data={
                'quantity': 82,
            },
            kwargs={
                'person_pk': logistics_person.pk,
                'shipping_pk': shipping_pk,
            },
        )
        self.assertEqual(resp.status_code, 200)

    def test_automovements_bulk_create(self):
        user, _ = self.login()
        logistics_person = self.get_new_logistica_person(user)

        region = self.get_or_create_one_region()
        maker = self.get_new_maker_person()
        maker.region = region
        maker.save()

        midpoint1 = self.get_midpoint_person()
        midpoint1.region = region
        midpoint1.save()

        midpoint2 = self.get_midpoint_person('midpoint2')
        midpoint2.region = region
        midpoint2.save()

        carrier = self.get_new_carrier_person()
        carrier.region = region
        carrier.save()

        resource1 = Resource.objects.create(name="visera1")
        resource2 = Resource.objects.create(name="visera2")
        resource3 = Resource.objects.create(name="visera3")
        resource4 = Resource.objects.create(name="visera4")
        resource5 = Resource.objects.create(name="visera5")

        # yapf: disable
        data = {
            'bulk': [
                {
                    'person': midpoint1.pk,
                    'source_region': region.pk,
                    'resource_bulk': [resource1.pk, resource2.pk, resource3.pk],
                    'carrier': carrier.pk,
                },
                {
                    'person': midpoint2.pk,
                    'source_region': region.pk,
                    'resource_bulk': [resource4.pk, resource5.pk],
                    'carrier': carrier.pk,
                }
            ]
        }
        # yapf: enable
        resp = self._create(
            urlname='middle_person_automovementconfig_c',
            data=data,
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 400)

        logistics_person.logistica_region_write.add(region)
        resp = self._create(
            urlname='middle_person_automovementconfig_c',
            data=data,
            kwargs={
                'person_pk': logistics_person.pk,
            },
        )
        self.assertEqual(resp.status_code, 201)
