import os
from django.apps import apps
from django.core.management.base import BaseCommand
from django.contrib.gis.utils import LayerMapping


class Command(BaseCommand):
    help = 'Carga datos Shape (.shp) en modelo de DB'

    def add_arguments(self, parser):
        parser.add_argument(
            '--shpfile',
            dest='shpfile',
            required=True,
            help='Archivo a cargar',
        )
        parser.add_argument(
            '--verbose',
            dest='verbose',
            required=False,
            help='Mostrar información extra',
            action="store_true",
        )
        parser.add_argument(
            '--model',
            dest='model',
            required=False,
            default='logistica.Region',
            help='Modelo donde cargar los datos (logistica.Region por defecto)',
        )

    def handle(self, *args, **options):
        # Determina path correcto
        file_path = os.path.abspath(options['shpfile'])
        # Separamos nombre de app de nombre de modelo
        _app_model = options['model'].split('.')
        # determina el nombre del mapping a partir del nombre del modelo
        # modelname_mapping
        _mapping_attr_txt = _app_model[1].lower() + '_mapping'
        # Obtiene la clase del modelo
        klass = apps.get_model(_app_model[0], _app_model[1])
        # Lanza LayerMapping para cargar datos del .shp al modelo
        lmap = LayerMapping(
            klass,
            file_path,
            getattr(klass, _mapping_attr_txt),
            transform=False,
        )
        lmap.save(strict=True, verbose=options['verbose'])
