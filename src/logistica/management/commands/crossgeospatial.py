from django.core.management.base import BaseCommand
from logistica.models import Region, Municipios

# Fuentes de datos usadas:
# https://opendata.esri.es/datasets/53229f5912e04f1ba6dddb70a5abeb72_0
# https://github.com/inigoflores/ds-codigos-postales


class Command(BaseCommand):
    help = 'Cruza datos entre DB de códigos postales y db de municipios del IGN'

    def handle(self, *args, **options):
        for region in Region.objects.all():
            name = None
            if not region.ine_code:
                continue
            ine_code_pad = str(region.ine_code).zfill(5)
            try:
                municipio = Municipios.objects.get(codigoine=ine_code_pad)
                name = municipio.nameunit
            except Municipios.DoesNotExist:
                municipios_geom = Municipios.objects.filter(
                    geom__bbcontains=region.geom)

                if not municipios_geom:
                    municipios_geom = Municipios.objects.filter(
                        geom__contained=region.geom)

                if not municipios_geom:
                    municipios_geom = Municipios.objects.filter(
                        geom__bboverlaps=region.geom)

                if not municipios_geom:
                    import pdb
                    pdb.set_trace()

                name = ''
                for municipio_geom in municipios_geom:
                    name = name + ' / ' + municipio_geom.nameunit

            region.name = name
            region.save()
