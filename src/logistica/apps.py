from django.apps import AppConfig


class LogisticaConfig(AppConfig):
    name = 'logistica'

    def ready(self):
        from . import signals as _signals
