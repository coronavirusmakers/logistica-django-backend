from import_export import resources
from import_export.fields import Field
from django.contrib.postgres.search import TrigramSimilarity
from logistica.models import Shipping, Person, Region


class ShippingResource(resources.ModelResource):
    pk = Field(
        attribute='id',
        column_name='ID',
    )
    created_date = Field(
        attribute='created_date',
        column_name='Fecha de creación',
    )
    uuid = Field(
        attribute='uuid',
        column_name='UUID',
    )
    status = Field(
        attribute='status',
        column_name='Estado (ID)',
    )
    status_name = Field(
        attribute='status',
        column_name='Estado (Nombre)',
    )
    carrier = Field(
        attribute='carrier__id',
        column_name='Transportista (ID)',
    )
    carrier__name = Field(
        attribute='carrier__name',
        column_name='Transportista',
    )
    inventory__owner__name = Field(
        attribute='inventory__owner__name',
        column_name='Origen (nombre)',
    )
    inventory__owner__address = Field(
        attribute='inventory__owner__address',
        column_name='Origen (dirección)',
    )
    inventory__owner__number = Field(
        attribute='inventory__owner__number',
        column_name='Origen (número)',
    )
    inventory__owner__city = Field(
        attribute='inventory__owner__city',
        column_name='Origen (ciudad)',
    )
    inventory__owner__province = Field(
        attribute='inventory__owner__province',
        column_name='Origen (provincia)',
    )
    inventory__owner__cp = Field(
        attribute='inventory__owner__cp',
        column_name='Origen (CP)',
    )
    inventory__owner__country = Field(
        attribute='inventory__owner__country',
        column_name='Origen (País)',
    )
    inventory__owner__phone = Field(
        attribute='inventory__owner__phone',
        column_name='Origen (Teléfono)',
    )
    request__consumer__name = Field(
        attribute='request__consumer__name',
        column_name='Destino (nombre)',
    )
    request__consumer__address = Field(
        attribute='request__consumer__address',
        column_name='Destino (dirección)',
    )
    request = Field(
        attribute='request__id',
        column_name='ID pedido',
    )
    quantity = Field(
        attribute='quantity',
        column_name='Cantidad',
    )
    move = Field(
        attribute='move__id',
        column_name="Movimiento (ID inventario destino)",
    )
    move__owner__name = Field(
        attribute='move__owner__name',
        column_name="Movimiento (Nombre destino)",
    )
    move__owner__name = Field(
        attribute='move__owner__name',
        column_name='Movimiento destino (nombre)',
    )
    move__owner__address = Field(
        attribute='move__owner__address',
        column_name='Movimiento destino (dirección)',
    )
    move__owner__number = Field(
        attribute='move__owner__number',
        column_name='Movimiento destino (número)',
    )
    move__owner__city = Field(
        attribute='move__owner__city',
        column_name='Movimiento destino (ciudad)',
    )
    move__owner__province = Field(
        attribute='move__owner__province',
        column_name='Movimiento destino (provincia)',
    )
    move__owner__cp = Field(
        attribute='move__owner__cp',
        column_name='Movimiento destino (CP)',
    )
    move__owner__country = Field(
        attribute='move__owner__country',
        column_name='Movimiento destino (País)',
    )
    move__owner__phone = Field(
        attribute='move__owner__phone',
        column_name='Movimiento destino (Teléfono)',
    )

    inventory__id = Field(
        attribute='inventory__pad_batch',
        column_name="ID del lote a recoger",
    )

    class Meta:
        model = Shipping

    def dehydrate_status_name(self, obj):
        return dict(obj.STATUS_CHOICES).get(obj.status)


class PersonResource(resources.ModelResource):
    class Meta:
        model = Person


class RegionResource(resources.ModelResource):
    class Meta:
        model = Region
        import_id_fields = (
            'postal_code',
            'name',
            'parent',
            'level',
            'id',
        )

    def get_or_init_instance(self, instance_loader, row):
        """ from doc:
            get_or_init_instance() is called with current BaseInstanceLoader
            and current row of the dataset, returning an object and a Boolean
            declaring if the object is newly created or not.

            If no object can be found for the current row, init_instance()
            is invoked to initialize an object.
        """

        postal_code = row.get('postal_code')
        _id = row.get('id')

        # if the document have an id in dataset, ignore postal_code match
        # if the document does not have id and have postal code, get
        # record by postal_code lookup.
        # if postal_code does not exists, call parent to create new one
        if not _id and postal_code:
            try:
                reg = Region.objects.get(postal_code=str(postal_code).strip())
                return (reg, False)
            except Region.MultipleObjectsReturned:
                # Si llega aquí, se han encontrado varias regiones
                # con el mismo código postal (es normal y puede pasar)
                # Determina entonces la región correcta por nombre haciendo
                # trigram search, si no la encuntra, es una región nueva
                # y se permite crearla
                name = str(row.get('name'))
                if name:
                    # yapf: disable
                    reg = Region.objects.filter(
                        postal_code=str(postal_code).strip(),
                    ).annotate(
                        similarity=TrigramSimilarity('name', name)
                    ).filter(similarity__gt=0.3).order_by('-similarity').first()
                    # yapf: enable
                    if reg:
                        return (reg, False)
                    # Si llega aquí, no se hace nada y se permite crear
                    # una entrada nueva
            except Region.DoesNotExist:
                # Si se llega aquí, no hay ningún código postal
                # existente en la db que haga match, se permite
                # crear uno nuevo
                pass
        return super().get_or_init_instance(instance_loader, row)
