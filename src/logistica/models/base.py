from django.core.cache import cache
from django.db.utils import ProgrammingError
from django.contrib.gis.db import models


def _refresh_global_config(instance):
    cache.set('_global_config_instance', instance, timeout=120)  # two minutes


class BaseModel(models.Model):
    created_date = models.DateTimeField(db_index=True, auto_now_add=True)
    last_updated_date = models.DateTimeField(db_index=True, auto_now=True)
    notes = models.TextField(
        null=True,
        blank=True,
        verbose_name='Notas',
    )

    class Meta:
        abstract = True


class LogisticaSiteConfig(models.Model):
    GLOBAL_CONFIG = 1
    CONFIG = ((GLOBAL_CONFIG, 'Global config'), )
    config_context = models.IntegerField(
        default=GLOBAL_CONFIG,
        choices=CONFIG,
        unique=True,
    )

    day_inventory_limit = models.IntegerField(
        default=10,
        verbose_name=
        'Cantidad de "Inventory" (lotes) que un maker puede colocar cada 24h',
    )

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        super().save(*args, **kwargs)
        _refresh_global_config(self)


def get_global_config():
    _cache_key = cache.get('_global_config_instance')
    if _cache_key:
        return _cache_key
    try:
        instance, _created = LogisticaSiteConfig.objects.using(
            'default').get_or_create(
                config_context=LogisticaSiteConfig.GLOBAL_CONFIG)
    except ProgrammingError:
        return None
    _refresh_global_config(instance)
    return cache.get('_global_config_instance')
