from django.contrib.gis.db import models
from logistica.models.base import BaseModel


class StatsByDay(models.Model):
    date = models.DateField(
        db_index=True,
        auto_now_add=False,
        auto_now=False,
        null=False,
        blank=False,
    )
    resource = models.ForeignKey(
        'Resource',
        null=False,
        related_name='stats_by_day',
        blank=False,
        on_delete=models.PROTECT,
        verbose_name='Recurso',
    )
    region = models.ForeignKey(
        'Region',
        null=False,
        related_name='stats_by_day',
        blank=False,
        on_delete=models.PROTECT,
    )

    build_static_value = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Cantidad estática fabricada',
        help_text='Cantidad introducida a mano de producto fabricado',
    )
    build_calculated_value = models.IntegerField(
        null=True,
        blank=True,
        default=0,
        verbose_name='Cantidad calculada',
        help_text='Cantidad calculada automáticamente. No rellenar.',
    )

    deliver_static_value = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name='Cantidad estática entregada',
        help_text='Cantidad introducida a mano de producto entregado',
    )
    deliver_calculated_value = models.IntegerField(
        null=True,
        blank=True,
        default=0,
        verbose_name='Cantidad calculada',
        help_text='Cantidad calculada automáticamente',
    )

    class Meta:
        unique_together = (
            'date',
            'region',
            'resource',
        )

    def __str__(self):
        return f"{self.build_static_value} -> {self.deliver_static_value}"
