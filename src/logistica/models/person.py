import operator
from functools import reduce
import logging
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.db.models import Q, Subquery
from django.core.exceptions import ValidationError
from django.contrib.gis.db import models
from fernet_fields import EncryptedCharField
from logistica.models.base import BaseModel
from logistica.models.region import Region

logger = logging.getLogger('logistica.models.person')  # pylint: disable=invalid-name


class BaseRoleFieldModel(BaseModel):
    # Producer
    ROLE_PRODUCER = 0
    # Salud. Hospital, residencia, etc.
    ROLE_CONSUMER = 1
    # Deprecated
    ROLE_LOGISTICS = 3
    # Transportista
    ROLE_CARRIER = 4
    # Punto intermedio
    ROLE_MIDPOINT = 8

    # Deprecated roles
    ROLE_DEPRECATED1 = 2
    ROLE_DEPRECATED2 = 5
    ROLE_DEPRECATED3 = 6
    ROLE_DEPRECATED4 = 7

    ROLE_CHOICES = (
        (ROLE_PRODUCER, 'Productor'),
        (ROLE_CONSUMER, 'Demandante'),
        (ROLE_LOGISTICS, 'Encargado de zona (logística)'),
        (ROLE_CARRIER, 'Transportista'),
        (ROLE_MIDPOINT, 'Punto intermedio'),
    )

    role = models.IntegerField(
        default=ROLE_PRODUCER,
        choices=ROLE_CHOICES,
        verbose_name='Role',
        db_index=True,
    )

    class Meta:
        abstract = True


class RoleDisambiguation(BaseRoleFieldModel):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name='Name',
    )

    def __str__(self):
        return f"(Role: {dict(self.ROLE_CHOICES).get(self.role)}) Desambiguación: {self.name}"


class Person(BaseRoleFieldModel):
    # WARNING: Si añades fields aquí, recuerda que en la admin
    # los fields están puestos a mano

    PERM_TRUE = True
    PERM_FALSE = False
    PERM_INHERIT = None

    PERM_CHOICES = (
        (PERM_TRUE, 'Permitir'),
        (PERM_FALSE, 'Denegar'),
        (PERM_INHERIT, 'Heredar configuración de la región'),
    )

    # Acceso lectura a roles de logistica
    logistica_region_readonly = models.ManyToManyField(
        'Region',
        blank=True,
        related_name='readonly_access_persons',
        verbose_name="Acceso de lectura a Región",
        help_text=
        'Si este person tiene role Encargado de zona, tendrá acceso de lectura a los objetos de las zonas seleccionadas',  # pylint: disable=line-too-long
    )
    # Acceso escritura a roles de logistica
    logistica_region_write = models.ManyToManyField(
        'Region',
        blank=True,
        related_name='write_access_persons',
        verbose_name="Acceso de lectura/escritura a Región",
        help_text=
        'Si este person tiene role Encargado de zona, tendrá acceso de lectura/escritura a los objetos de las zonas seleccionadas. Este campo prevalece sobre el de sólo lectura',  # pylint: disable=line-too-long
    )
    logistica_full_access = models.BooleanField(
        default=False,
        verbose_name="Acceso completo",
        help_text='Da acceso a todas las regiones',
    )
    logistica_recursive_access = models.BooleanField(
        default=False,
        verbose_name="Acceso recursivo",
        help_text='Da acceso recursivo de las regiones asignadas',
    )
    person_allow_set_send_status = models.BooleanField(
        default=PERM_INHERIT,
        null=True,
        blank=True,
        choices=PERM_CHOICES,
        verbose_name=
        "Permitir setear estado (shipping) enviado por parte del person",
    )
    person_allow_set_received_status = models.BooleanField(
        default=PERM_INHERIT,
        null=True,
        blank=True,
        choices=PERM_CHOICES,
        verbose_name=
        "Permitir setear estado (shipping) recibido por parte del person",
    )
    can_make_orders = models.BooleanField(
        default=True,
        verbose_name="Puede realizar pedidos",
        help_text=
        'Da acceso a realizar pedidos, tanto para sí mismo como para otros (en caso de role logística)',
    )
    bypass_inventory_day_limit = models.BooleanField(
        default=True,
        verbose_name="Ignorar límite de lotes diario",
        help_text=
        'Saltar la limitación de creación de Inventory (lotes) al día por un maker',
    )

    role_disambiguation = models.ForeignKey(
        'RoleDisambiguation',
        null=False,
        related_name='persons',
        blank=False,
        on_delete=models.PROTECT,
    )

    region = models.ForeignKey(
        'Region',
        null=False,
        related_name='person_points',
        blank=False,
        on_delete=models.PROTECT,
    )

    telegram_nick = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Telegram nick',
        help_text='Este campo no se valida, no confiar',
    )
    telegram_id = models.BigIntegerField(
        null=True,
        blank=True,
        verbose_name='Telegram id',
        help_text='Este campo no se valida, no confiar',
    )

    # Encrypted fields
    name = EncryptedCharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name='Name',
    )
    contact_person_name = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Nombre de persona de contacto',
    )
    address = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Dirección, calle',
    )
    number = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Número de calle',
    )
    city = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Ciudad',
    )
    province = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Provincia',
    )
    cp = EncryptedCharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name='Código postal',
    )
    country = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='País',
    )
    phone = EncryptedCharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Teléfono',
    )
    # End of encrypted fields

    point = models.PointField(
        verbose_name='Geospatial point',
        spatial_index=True,
        null=True,
        blank=True,
        # 4326 == WGS84
        # https://en.wikipedia.org/wiki/World_Geodetic_System#WGS84
        srid=4326,
    )
    production_capacity = models.IntegerField(
        default=1,
        verbose_name='Capacidad de producción',
    )
    machine_models = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name='Modelo/s de impresora',
    )
    machine_count = models.IntegerField(
        default=0,
        verbose_name='Cantidad de impresoras',
    )
    logistics_need = models.BooleanField(
        default=False,
        verbose_name="Necesidad logística",
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name='persons',
        blank=True,
        default=None,
        on_delete=models.SET_NULL,
        verbose_name="Usuario real asociado",
    )

    def logistica_can_read_region(self, start_region):
        """Determine if a person of logistics can read in a region

        Args:
            start_region (Region): Start region. Check will be made upwards
                recursively.

        Returns:
            bool: can or not
        """
        if self.role != self.ROLE_LOGISTICS:
            return False
        if self.logistica_full_access:
            return True
        try:
            region = start_region
            panic_exit_loop = 0
            while True:  # WARNING: infinite loop
                panic_exit_loop += 1
                if panic_exit_loop > 100:
                    logger.warning(
                        "Can read region perm check killed after loop 100")
                    return False
                if self.logistica_region_readonly.filter(
                        pk=region.pk).exists():
                    return True
                if self.logistica_region_write.filter(pk=region.pk).exists():
                    return True
                region = region.parent
        except Person.region.RelatedObjectDoesNotExist:
            pass
        except Region.parent.RelatedObjectDoesNotExist:
            pass
        except AttributeError:
            pass
        return False

    def logistica_can_write_region(self, start_region):
        """Determine if a person of logistics can write in a region

        Args:
            start_region (Region): Start region. Check will be made upwards
                recursively.

        Returns:
            bool: can or not
        """
        if self.role != self.ROLE_LOGISTICS:
            return False
        if self.logistica_full_access:
            return True
        try:
            region = start_region
            panic_exit_loop = 0
            while True:  # WARNING: infinite loop
                panic_exit_loop += 1
                if panic_exit_loop > 100:
                    logger.warning(
                        "Can write region perm check killed after loop 100")
                    return False
                if self.logistica_region_write.filter(pk=region.pk).exists():
                    return True
                region = region.parent
        except Person.region.RelatedObjectDoesNotExist:
            pass
        except Region.parent.RelatedObjectDoesNotExist:
            pass
        except AttributeError:
            pass
        return False

    def _with_parents_logistics_allowed_read_regions(self):
        # yapf: disable
        all_querysets = list()
        # Asumimos que no van a existir más de 10 niveles
        for level in range(10, 0, -1):
            queryset = Region.objects.filter(Q(
                Q(
                    **{("parent__" * (10 - level)) + 'readonly_access_persons': self},
                ) |
                Q(
                    **{("parent__" * (10 - level)) + 'write_access_persons': self},
                ),
            ))
            all_querysets.append(queryset)
        return reduce(operator.or_, all_querysets)
        # yapf: enable

    def _with_parents_logistics_allowed_write_regions(self):
        # yapf: disable
        all_querysets = list()
        # Asumimos que no van a existir más de 10 niveles
        for level in range(10, 0, -1):
            queryset = Region.objects.filter(
                **{("parent__" * (10 - level)) + 'readonly_access_persons': self},
            )
            all_querysets.append(queryset)
        return reduce(operator.or_, all_querysets)
        # yapf: enable

    def logistics_allowed_read_regions(self, include_parents=False):
        """Obtains a queryset list of regions wich the person can write

        Args:
            include_parents (bool, optional): Include regions with level > 1.
                Defaults to False.

        Returns:
            Queryset: Regions queryset
        """
        if include_parents:
            return self._with_parents_logistics_allowed_read_regions()
        if self.logistica_full_access:
            return Region.objects.all()
        last_queryset = None
        # Asumimos que no van a existir más de 10 niveles
        for level in range(10, 0, -1):
            # yapf: disable
            q_filter = Q(
                Q(
                    readonly_access_persons=self,
                ) |
                Q(
                    write_access_persons=self,
                ),
                level=level
            )
            if last_queryset is not None:
                q_filter = q_filter | Q(
                    parent__in=Subquery(last_queryset.values('pk')),
                )
            last_queryset = Region.objects.filter(q_filter)
            # yapf: enable
        return last_queryset.distinct()

    def logistics_allowed_write_regions(self, include_parents=False):
        """Obtains a queryset list of the regions wich the person can write

        Args:
            include_parents (bool, optional): Include regions with level > 1.
                Defaults to False.

        Returns:
            Queryset: Regions queryset
        """
        if include_parents:
            return self._with_parents_logistics_allowed_write_regions()
        if self.logistica_full_access:
            return Region.objects.all()
        last_queryset = None
        # Asumimos que no van a existir más de 10 niveles
        for level in range(10, 0, -1):
            # yapf: disable
            q_filter = Q(
                write_access_persons=self,
                level=level
            )
            if last_queryset is not None:
                q_filter = q_filter | Q(
                    parent__in=Subquery(last_queryset.values('pk'))
                )
            last_queryset = Region.objects.filter(
                q_filter | Q(write_access_persons=self)
            )
            # yapf: enable
        return last_queryset.distinct()

    @property
    def can_set_received_status(self):
        """Determine if this person can set received status of shipping

        Returns:
            bool: yes or not
        """
        if self.person_allow_set_received_status is not None:
            return self.person_allow_set_received_status
        try:
            region = self.region
            panic_exit_loop = 0
            while True:  # WARNING: infinite loop
                panic_exit_loop += 1
                if panic_exit_loop > 100:
                    logger.warning(
                        "Can set received status perm check killed after loop 100"
                    )
                    return False
                if region.default_person_allow_set_received_status is not None:
                    return region.default_person_allow_set_received_status
                region = region.parent
        except Person.region.RelatedObjectDoesNotExist:
            pass
        except Region.parent.RelatedObjectDoesNotExist:
            pass
        except AttributeError:
            pass
        return False

    @property
    def can_set_send_status(self):
        """Determine if this person can set sent status of shipping

        Returns:
            bool: yes or not
        """
        if self.person_allow_set_send_status is not None:
            return self.person_allow_set_send_status
        try:
            region = self.region
            panic_exit_loop = 0
            while True:  # WARNING: infinite loop
                panic_exit_loop += 1
                if panic_exit_loop > 100:
                    logger.warning(
                        "Can set send status perm check killed after loop 100")
                    return False
                if region.default_person_allow_set_send_status is not None:
                    return region.default_person_allow_set_send_status
                region = region.parent
        except Person.region.RelatedObjectDoesNotExist:
            pass
        except Region.parent.RelatedObjectDoesNotExist:
            pass
        except AttributeError:
            pass
        return False

    def clean(self):
        super().clean()
        if self.role_disambiguation.role != self.role:
            raise ValidationError({
                'role_disambiguation':
                _('role and disambiguation role should be the same.'),
                'role':
                _('role and disambiguation role should be the same.'),
            })

        if self.region.level > 1:
            raise ValidationError({
                'region':
                _('A Person record cannot be inside region with level greater than 1.'
                  ),
            })

    def __str__(self):
        return f"(Role: {dict(self.ROLE_CHOICES).get(self.role)}) {self.name}"
