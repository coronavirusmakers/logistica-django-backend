from django.conf.urls import url
from .classviews import basic as basicviews

urlpatterns = [
    # Endpoints básicos, accesibles por un usuario normal cuando
    # se registra
    # Persons de cualqueir tipo
    url(
        r'^person/$',
        basicviews.BasicPersonLC.as_view(),
        name='basic_person_list_create',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/$',
        basicviews.BasicPersonRUD.as_view(),
        name='basic_person_retrieve_update_delete',
    ),
    # Inventory
    url(
        r'^person/(?P<person_pk>\d+)/inventory/$',
        basicviews.BasicInventoryLC.as_view(),
        name='basic_person_inventory_list_create',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/inventory/(?P<inventory_pk>\d+)/$',
        basicviews.BasicInventoryRUD.as_view(),
        name='basic_person_inventory_retrieve_update_delete',
    ),
    # Resources
    url(
        r'^resource/$',
        basicviews.BasicResourceL.as_view(),
        name='basic_resource_list_create',
    ),
    url(
        r'^resource/(?P<resource_pk>\d+)/$',
        basicviews.BasicResourceR.as_view(),
        name='basic_resource_list_create',
    ),
    # Regions
    url(
        r'^region/$',
        basicviews.BasicRegionL.as_view(),
        name='basic_region_list',
    ),
    # Roles
    url(
        r'^role/$',
        basicviews.BasicRoleL.as_view(),
        name='basic_role_list',
    ),
    url(
        r'^role/(?P<role_id>\d+)/disambiguation/$',
        basicviews.BasicRoleDisambiguationL.as_view(),
        name='basic_role_disambiguation_list',
    ),
    # Pedidos. Sólo podrá crearlos y obtenerlos, pero no
    # modificarlos
    url(
        r'^person/(?P<person_pk>\d+)/request/$',
        basicviews.BasicRequestLC.as_view(),
        name='basic_person_request_list_create',
    ),
    url(
        r'^person/(?P<person_pk>\d+)/request/(?P<request_pk>\d+)/$',
        basicviews.BasicRequestRU.as_view(),
        name='basic_person_request_ru',
    ),
    # Algunos persons pueden setear el status de un shipping
    url(
        r'^person/(?P<person_pk>\d+)/shipping/(?P<shipping_pk>\d+)/$',
        basicviews.BasicShippingU.as_view(),
        name='basic_person_shipping_update',
    ),
]
