import operator
from functools import reduce
from django.contrib.gis.geos import Point
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models import Q, Sum, F
from django.db.models.functions import Coalesce
from django.contrib.gis.db.models.functions import Distance
from rest_framework.exceptions import ParseError
import django_filters as filters
from logistica import models


# HACK: Estamos usando django 2.2 por ser LTE, que no soporta distinct en
# Sum. Por ello aquí reproducimos el commit de django 3.0 que añade soporte
# para distinct en Sum:
# https://github.com/django/django/commit/5f24e7158e1d5a7e40fa0ae270639f6a171bb18e
# En caso de hacer upgrade a django >= 3.0, esto ya no sería necesario
class SumDistinctHACK(Sum):  # pylint: disable=abstract-method
    allow_distinct = True


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class PersonFilterSet(filters.FilterSet):
    distance = filters.CharFilter(
        method='filter_distance',
        help_text=
        ("Añade campo distance en los objetos con respecto al punto indicado"
         " Formato: lng;lat. Ejemplo: 25.7040066471688;-100.31368999414681"
         " Use el campo de ordering con 'distance' o '-distance' para ordenar"
         "por distancia de mayor a menor o de menor a mayor"),
    )

    def filter_distance(self, queryset, _name, value):
        try:
            lng, lat = (float(n) for n in value.split(';'))
        except ValueError:
            raise ParseError(
                'Formato de distancia erróneo. Use lng;lat. Ejemplo: 25.7040066471688;-100.31368999414681'
            )

        pnt = Point(lat, lng, srid=4326)
        res = queryset.annotate(distance=Distance('point', pnt))
        ordering = self.request.query_params.get('ordering')
        if ordering and ('distance' in ordering or '-distance' in ordering):
            return res.order_by(ordering)
        return res


class InventoryFilterSet(filters.FilterSet):
    distance = filters.CharFilter(
        method='filter_distance',
        help_text=
        ("Añade campo distance en los objetos con respecto al punto indicado"
         " Formato: lon;lat. Ejemplo: 25.7040066471688;-100.31368999414681"
         " Use el campo de ordering con 'distance' o '-distance' para ordenar"
         "por distancia de mayor a menor o de menor a mayor"),
    )
    with_avail_quantity = filters.BooleanFilter(
        method='filter_with_avail_quantity',
        help_text="true: inventario con stock disponible, false: no hace nada",
    )

    owner__region__in = NumberInFilter(
        field_name='owner__region__pk',
        lookup_expr='in',
        help_text='1,2,3,4',
    )

    owner__region__gt1level = filters.CharFilter(
        method='filter_owner__region__gt1level',
        help_text='Filtra recursivamente por regiones de nivel mayor a 1',
    )

    def filter_owner__region__gt1level(self, queryset, _name, value):
        def q_one_region_pk(region_pk):
            try:
                region = models.Region.objects.get(pk=int(region_pk))
            except models.Region.DoesNotExist:
                return None
            if region.level == 1:
                return None
            uplevels = region.level - 1
            query_key = 'owner__region' + ('__parent' * uplevels)
            return Q(**{query_key: region})

        values = value.split(',')
        if not values:
            return queryset

        q_queries = list()
        for region_pk in values:
            q_query = q_one_region_pk(region_pk.strip())
            if q_query is not None:
                q_queries.append(q_query)
        if q_queries:
            return queryset.filter(Q(reduce(operator.or_, q_queries)))
        return queryset

    def filter_distance(self, queryset, _name, value):
        try:
            lng, lat = (float(n) for n in value.split(';'))
        except ValueError:
            raise ParseError(
                'Formato de distancia erróneo. Use lng;lat. Ejemplo: 25.7040066471688;-100.31368999414681'
            )

        pnt = Point(lat, lng, srid=4326)
        res = queryset.annotate(distance=Distance('owner__point', pnt))
        ordering = self.request.query_params.get('ordering')
        if ordering and ('distance' in ordering or '-distance' in ordering):
            return res.order_by(ordering)
        return res

    def filter_with_avail_quantity(self, queryset, _name, value):
        if not value:
            return queryset
        # asumimos que la operación annotate+Sum se ha realizado
        # en el view
        return queryset.filter(sum_quantity_available__gt=0)

    class Meta:
        model = models.Inventory
        fields = (
            'distance',
            'owner__role',
            'owner__region__in',
            'owner__region',
            'resource__resource_type',
            'resource',
            'with_avail_quantity',
            'owner__region__gt1level',
        )


class MiddleRequestFilterSet(filters.FilterSet):
    with_shippings = filters.BooleanFilter(
        method='filter_with_shippings',
        help_text=
        "true: pedidos con shippings asignados, false: pedidos sin shipping asignado",
    )
    shipping_needs = filters.BooleanFilter(
        method='filter_shipping_needs',
        help_text=
        "true: pedidos que tengan o no shippings, pero que éstos no satisfagan completamente la necesidad, false: no hace nada",  # pylint: disable=line-too-long
    )

    consumer__region__in = NumberInFilter(
        field_name='consumer__region__pk',
        lookup_expr='in',
        help_text='1,2,3,4',
    )

    consumer__region__gt1level = filters.CharFilter(
        method='filter_consumer__region__gt1level',
        help_text='Filtra recursivamente por regiones de nivel mayor a 1',
    )

    def filter_consumer__region__gt1level(self, queryset, _name, value):
        def q_one_region_pk(region_pk):
            try:
                region = models.Region.objects.get(pk=int(region_pk))
            except models.Region.DoesNotExist:
                return None
            if region.level == 1:
                return None
            uplevels = region.level - 1
            query_key = 'consumer__region' + ('__parent' * uplevels)
            return Q(**{query_key: region})

        values = value.split(',')
        if not values:
            return queryset

        q_queries = list()
        for region_pk in values:
            q_query = q_one_region_pk(region_pk.strip())
            if q_query is not None:
                q_queries.append(q_query)
        if q_queries:
            return queryset.filter(Q(reduce(operator.or_, q_queries)))
        return queryset

    def filter_with_shippings(self, queryset, _name, value):
        return queryset.filter(shippings__isnull=(not value))

    def filter_shipping_needs(self, queryset, _name, value):
        if not value:
            return queryset
        # filtra los pedidos cuando la cantidad sumada del shipping sea
        # menor a la cantidad solicitad en el pedido. Se asume que el
        # queryset principal ha hecho annotate de quantity_satisfied
        # yapf: disable
        res = queryset.filter(
            Q(
                Q(quantity__gt=F('quantity_satisfied'))
                | Q(shippings__isnull=True)
            )
        ).distinct()
        # yapf: enable
        # import pdb
        # pdb.set_trace()
        return res

    class Meta:
        model = models.Request
        fields = [
            'status',
            'consumer__role',
            'shippings__status',
            'with_shippings',
            'shipping_needs',
            'shippings__inventory__owner__role',
            'consumer__region__gt1level',
            'consumer__region__in',
        ]


class RegionFilterSet(filters.FilterSet):
    csv_search = filters.CharFilter(
        method='filter_csv_search',
        help_text="busqueda por términos separados por coma",
    )

    trigram_search = filters.CharFilter(
        method='filter_trigram_search',
        help_text="busqueda por términos separados por coma",
    )

    def filter_trigram_search(self, queryset, _name, value):
        postal_code = "".join([str(s) for s in value.split() if s.isdigit()])
        text = "".join([str(s) for s in value.split() if not s.isdigit()])

        # yapf: disable
        if not text:
            return queryset.filter(postal_code__icontains=postal_code)

        if not postal_code:
            return queryset.annotate(
                similarity=TrigramSimilarity('name', text)
            ).filter(similarity__gt=0.3).order_by('-similarity')

        return queryset.annotate(
            similarity=TrigramSimilarity('name', text)
        ).filter(similarity__gt=0.3).filter(
            postal_code__icontains=postal_code
        ).order_by('-similarity')
        # yapf: enable

    def filter_csv_search(self, queryset, _name, value):
        values = value.split(',')
        # yapf: disable
        return queryset.filter(
            Q(reduce(operator.or_, (Q(postal_code__icontains=x) for x in values)))
            | Q(reduce(operator.or_, (Q(name__icontains=x) for x in values)))
        )
        # yapf: enable

    class Meta:
        model = models.Region
        fields = [
            'postal_code',
            'name',
            'csv_search',
            'trigram_search',
        ]


class MiddleShippingFilterSet(filters.FilterSet):
    EXPORT_XLS = 0
    EXPORT_CHOICES = ((EXPORT_XLS, 'XLS'), )
    move__isnull = filters.BooleanFilter(
        field_name='move',
        lookup_expr='isnull',
    )

    export = filters.ChoiceFilter(
        choices=EXPORT_CHOICES,
        method='filter_export',
        help_text="0=xls",
    )

    created_date = filters.DateTimeFromToRangeFilter()

    def filter_export(self, queryset, _name, _value):
        # Do nothing :)
        return queryset

    class Meta:
        model = models.Shipping
        fields = [
            'status',
            'move',
            'move',
            'request__consumer__role',
            'move__owner__role',
            'inventory__owner__role',
            'status',
            'export',
            'created_date',
        ]
