from django.conf.urls import url
from .classviews import stats as statsviews

urlpatterns = [
    # Endpoints estadísticas
    #
    #
    url(
        r'^resource/buildtotal/$',
        statsviews.StatsBuildResourceL.as_view(),
        name='stats_build_total_list',
    ),
    url(
        r'^resource/consumedtotal/$',
        statsviews.StatsConsumedResourceL.as_view(),
        name='stats_consumed_total_list',
    ),
    url(
        r'^resource/bydayresources/$',
        statsviews.StatsByDayResourcesL.as_view(),
        name='stats_bydayresources_list',
    ),
    url(
        r'^resource/bydaysumresources/$',
        statsviews.StatsByDaySumResourcesL.as_view(),
        name='stats_bydaysumresources_list',
    ),
]
