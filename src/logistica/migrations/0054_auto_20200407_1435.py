# Generated by Django 2.2.11 on 2020-04-07 14:35

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0053_auto_20200407_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='uuid',
            field=models.CharField(db_index=True, default=uuid.uuid4, editable=False, help_text='UUID', max_length=36, unique=True),
        ),
    ]
