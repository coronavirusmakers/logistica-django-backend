# Generated by Django 2.2.11 on 2020-04-05 20:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0043_auto_20200405_1927'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='default_person_allow_set_received_status',
            field=models.BooleanField(default=False, help_text='Permitir por defecto setear estado recibido (de shipping) por parte del receptor de un pedido. Esto se aplicará sólo a los nuevos persons creados.', verbose_name='Permiso de estado recibido de shipping por defecto'),
        ),
        migrations.AlterField(
            model_name='region',
            name='default_person_allow_set_send_status',
            field=models.BooleanField(default=False, help_text='Permitir por defecto setear estado enviado (de shipping) por parte del dueño del inventario. Esto se aplicará sólo a los nuevos persons creados.', verbose_name='Permiso de estado enviado de shipping por defecto'),
        ),
    ]
