# Generated by Django 2.2.11 on 2020-03-23 12:05

from django.db import migrations, models
import uuid

class Migration(migrations.Migration):

    dependencies = [
        ('logistica', '0013_auto_20200322_2108'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='photo_link',
            field=models.URLField(blank=True, null=True, verbose_name='Foto'),
        ),
        migrations.AddField(
            model_name='inventory',
            name='track_batch',
            field=models.PositiveIntegerField(default=0, help_text='En caso de recibir este producto desde otro proveedor, indica el Nº de lote recibido', verbose_name='Nº de lote original'),
        ),
        migrations.AddField(
            model_name='resource',
            name='resource_type',
            field=models.IntegerField(choices=[(0, 'Material para maker'), (1, 'Parte de producto'), (2, 'Producto terminado')], default=0, verbose_name='Tipo de recurso'),
        ),
        migrations.AddField(
            model_name='resource',
            name='sku',
            field=models.CharField(default='---', max_length=250, verbose_name='SKU'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='resource',
            name='spl_link',
            field=models.URLField(blank=True, null=True, verbose_name='SPL link'),
        ),
        migrations.AddField(
            model_name='shipping',
            name='carrier_track_num',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Track num del transportista'),
        ),
        migrations.AddField(
            model_name='shipping',
            name='uuid',
            field=models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, help_text='Nº único de envío', null=True),
        ),
    ]
