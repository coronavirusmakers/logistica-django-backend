# Generated by Django 2.2.11 on 2020-04-14 18:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authc', '0001_initial'),
        ('logistica', '0061_auto_20200413_1712'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='custom_user',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='persons', to='authc.User', verbose_name='Usuario real asociado'),
        ),
    ]
