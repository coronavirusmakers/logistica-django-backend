from django.contrib.gis import admin
from django.utils.safestring import mark_safe
from django.contrib.admin.filters import SimpleListFilter
from dynamic_raw_id.admin import DynamicRawIDMixin
from dynamic_raw_id.filters import DynamicRawIDFilter
from import_export.admin import ImportExportModelAdmin
from logistica.exports import PersonResource, RegionResource
from logistica.models import (
    Region,
    Resource,
    Inventory,
    Person,
    Shipping,
    Tracking,
    Request,
    LogisticaSiteConfig,
    StatsByDay,
    RoleDisambiguation,
    AutoMovementConfiguration,
)

admin.site.register(RoleDisambiguation, admin.OSMGeoAdmin)
# admin.site.register(AutoMovementConfiguration, admin.OSMGeoAdmin)
# admin.site.register(Region, admin.OSMGeoAdmin)
admin.site.register(Resource, admin.OSMGeoAdmin)
# admin.site.register(StatsByDay, admin.OSMGeoAdmin)
# admin.site.register(Batch, admin.OSMGeoAdmin)
# admin.site.register(Inventory, admin.OSMGeoAdmin)
# admin.site.register(Person, admin.OSMGeoAdmin)
# admin.site.register(Shipping, admin.OSMGeoAdmin)
admin.site.register(Tracking, admin.OSMGeoAdmin)
admin.site.register(LogisticaSiteConfig, admin.OSMGeoAdmin)

# admin.site.register(Request, admin.OSMGeoAdmin)

# admin.site.register(Carrier, admin.OSMGeoAdmin)


@admin.register(AutoMovementConfiguration)
class AutoMovementConfigurationAdmin(DynamicRawIDMixin, admin.OSMGeoAdmin):
    search_fields = (
        'id',
        'source_region__name',
        'target_inventory__owner__name',
        'carrier__name',
    )
    dynamic_raw_id_fields = (
        'source_region',
        'target_inventory',
        'carrier',
    )
    list_display = (
        'pk',
        '__str__',
        'source_region',
        'target_inventory',
        'carrier',
    )


class InventoryPersonFilter(SimpleListFilter):
    title = 'person type'
    parameter_name = 'person_role'
    default_value = None

    def lookups(self, request, model_admin):
        return Person.ROLE_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(owner__role=self.value())
        return queryset


@admin.register(Region)
class RegionAdmin(admin.OSMGeoAdmin, DynamicRawIDMixin,
                  ImportExportModelAdmin):
    resource_class = RegionResource
    dynamic_raw_id_fields = ('parent', )
    list_display = (
        'id',
        'level',
        'postal_code',
        'name',
        'parent',
    )
    search_fields = (
        'id',
        'postal_code',
        'name',
    )


@admin.register(Inventory)
class InventoryAdmin(DynamicRawIDMixin, admin.OSMGeoAdmin):
    search_fields = (
        'id',
        'resource__name',
    )
    dynamic_raw_id_fields = (
        'resource',
        'owner',
    )
    list_display = (
        'pk',
        '__str__',
        'quantity',
        'get_region',
        'get_person',
        'get_quantity_available',
    )
    list_filter = (InventoryPersonFilter, )

    def get_region(self, obj):
        return obj.owner.region

    def get_person(self, obj):
        return str(obj.owner)

    def get_quantity_available(self, obj):
        return obj.quantity_available

    get_region.short_description = 'Region'
    get_person.short_description = 'Persona'
    get_quantity_available.short_description = 'Cantidad DISPONIBLE'


class TrackingInline(DynamicRawIDMixin, admin.TabularInline):
    model = Tracking
    fieldsets = (
        (None, {
            'fields': ('status', 'scheduled_date', 'record_date')
        }),
        ('Actor en este evento', {
            'fields': ('person', )
        }),
    )
    dynamic_raw_id_fields = ('person', )
    extra = 0


class ShippingStatusFilter(SimpleListFilter):
    title = 'status'
    parameter_name = 'status'
    default_value = None

    def lookups(self, request, model_admin):
        return Shipping.STATUS_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(status=self.value())
        return queryset


class ShippingResourceFilter(SimpleListFilter):
    title = 'resource'
    parameter_name = 'resource'
    default_value = None

    def lookups(self, request, model_admin):
        list_of_resources = []
        queryset = Resource.objects.all()
        for resource in queryset:
            list_of_resources.append((str(resource.id), resource.name))
        return sorted(list_of_resources, key=lambda tp: tp[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(inventory__resource__pk=self.value())
        return queryset


@admin.register(Shipping)
class ShippingAdmin(DynamicRawIDMixin, admin.OSMGeoAdmin):
    search_fields = (
        'id',
        'uuid',
        'request__consumer__region__name',
        'inventory__owner__region__name',
        'move__owner__region__name',
    )
    list_display = (
        'pk',
        'carrier',
        'status',
        'get_tracking',
    )
    dynamic_raw_id_fields = (
        'request',
        'inventory',
        'consumer',
    )
    list_filter = (
        ShippingStatusFilter,
        ShippingResourceFilter,
    )
    inlines = (TrackingInline, )

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        form.base_fields['carrier'].queryset = Person.objects.filter(
            role=Person.ROLE_CARRIER)
        return form

    def get_tracking(self, obj):
        tracking_list = list()
        for tracking in obj.tracking.all().order_by('-record_date'):
            tracking_list.append(str(tracking))
        return mark_safe("<br />".join(tracking_list))

    get_tracking.allow_tags = True
    get_tracking.short_description = 'Tracking'


class InventoryInline(admin.TabularInline):
    model = Inventory
    fields = (
        'resource',
        'owner',
        'quantity',
    )
    extra = 0


class RequestInline(admin.TabularInline):
    model = Request
    fields = (
        'consumer',
        'resource',
        'quantity',
        'done',
    )
    extra = 0


class PersonRequestAttentionFilter(SimpleListFilter):
    title = 'request attention'
    parameter_name = 'request_attention'
    default_value = None

    def lookups(self, request, model_admin):
        return ((False, 'Necesita material'), )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(requests__done=self.value()).distinct()
        return queryset


class RequestAttentionFilter(SimpleListFilter):
    title = 'request attention'
    parameter_name = 'request_attention'
    default_value = None

    def lookups(self, request, model_admin):
        return (
            (True, 'Necesidad satisfecha'),
            (False, 'Necesidad'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(done=self.value()).distinct()
        return queryset


class PersonRoleFilter(SimpleListFilter):
    title = 'role'
    parameter_name = 'role'
    default_value = None

    def lookups(self, request, model_admin):
        return Person.ROLE_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(role=self.value())
        return queryset


@admin.register(Person)
class PersonAdmin(DynamicRawIDMixin, ImportExportModelAdmin,
                  admin.OSMGeoAdmin):

    resource_class = PersonResource
    dynamic_raw_id_fields = (
        'region',
        'logistica_region_readonly',
        'logistica_region_write',
    )
    search_fields = (
        'id',
        'inventories__resource__name',
        'telegram_nick',
        'telegram_id',
    )
    list_display = (
        'name',
        'pk',
        'telegram_nick',
        'region',
        'point',
        'production_capacity',
        'machine_count',
        'logistica_full_access',
        'get_read_write_region',
        'get_read_region',
        'logistics_need',
        'get_request_attention',
    )
    list_filter = (
        ('region', DynamicRawIDFilter),
        PersonRequestAttentionFilter,
        PersonRoleFilter,
    )
    inlines = (
        InventoryInline,
        RequestInline,
    )

    fieldsets = (
        ('Basic', {
            'fields': (
                'role',
                'role_disambiguation',
                'name',
                'region',
                'user',
                'can_make_orders',
                'person_allow_set_send_status',
                'person_allow_set_received_status',
            )
        }),
        ('Social', {
            'fields': (
                'telegram_nick',
                'telegram_id',
            )
        }),
        ('Datos', {
            'fields': (
                'address',
                'number',
                'city',
                'province',
                'cp',
                'country',
                'phone',
                'point',
            )
        }),
        ('Sólo role maker', {
            'fields': (
                'logistics_need',
                'production_capacity',
                'machine_models',
                'machine_count',
                'bypass_inventory_day_limit',
            )
        }),
        ('Sólo role logística', {
            'fields': (
                'logistica_region_readonly',
                'logistica_region_write',
                'logistica_full_access',
            )
        }),
    )

    def get_read_write_region(self, obj):
        region_list = list()
        for region in obj.logistica_region_write.all()[:10]:
            region_list.append(str(region))
        return mark_safe(", ".join(region_list))

    def get_read_region(self, obj):
        region_list = list()
        for region in obj.logistica_region_readonly.all()[:10]:
            region_list.append(str(region))
        return mark_safe(", ".join(region_list))

    def get_request_attention(self, obj):
        request_list = list()
        for request in obj.requests.filter(done=False):
            request_list.append(str(request))
        return mark_safe("<br />".join(request_list))

    get_request_attention.short_description = 'Necesides'
    get_read_write_region.short_description = '(logística) Puede leer/escribir en región'
    get_read_region.short_description = '(logística) Puede SOLO leer en región'


# @admin.register(Carrier)
# class CarrierAdmin(admin.OSMGeoAdmin):
#     search_fields = (
#         'id',
#         'name',
#         'address',
#         'phone',
#     )
#     list_display = (
#         'name',
#         'pk',
#         'region',
#         'address',
#         'phone',
#     )


@admin.register(Request)
class RequestAdmin(admin.OSMGeoAdmin):
    search_fields = ('id', )
    list_display = (
        'consumer',
        'pk',
        'resource',
        'quantity',
        'done',
    )
    list_filter = (RequestAttentionFilter, )

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        form.base_fields['consumer'].queryset = Person.objects.filter(
            role__in=(
                Person.ROLE_PRODUCER,
                Person.ROLE_CONSUMER,
            ))
        return form


@admin.register(StatsByDay)
class StatsByDayAdmin(DynamicRawIDMixin, admin.OSMGeoAdmin):
    search_fields = (
        'id',
        'resource__name',
        'region__name',
    )
    list_display = (
        'pk',
        '__str__',
        'date',
        'resource',
        'region',
        'build_static_value',
        'build_calculated_value',
        'deliver_static_value',
        'deliver_calculated_value',
    )
    list_filter = (('region', DynamicRawIDFilter), )
