"""
Django settings for cv19 logistica project.

Generated by 'django-admin startproject' using Django 2.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""
# yapf: disable
import os
from datetime import timedelta
from decouple import config

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', default=False, cast=bool)

ALLOWED_HOSTS = ["*"]

# Application definition

INSTALLED_APPS = [
    'material.admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    # rest
    'corsheaders',
    'rest_framework',
    'djoser',
    'rest_framework_gis',
    'django_filters',
    'drf_yasg',
    # postgis
    'django.contrib.gis',
    # fields dinámicos en la admin
    'dynamic_raw_id',
    # apps
    'authc.apps.AuthConfigC',
    'logistica.apps.LogisticaConfig',
    'front.apps.FrontConfig',
    'bootstrap4',
    'import_export',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        # Backend para utilizar PostGIS que añade funcionalidades
        # de objetos geográficos a la DB
        # https://docs.djangoproject.com/en/2.2/ref/contrib/gis/tutorial/#configure-settings-py
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        #
        # Backend normal de postgresql
        # 'ENGINE': 'django.db.backends.postgresql',
        'NAME': config('POSTGRES_DB'),
        'USER': config('POSTGRES_USER'),
        'PASSWORD': config('POSTGRES_PASSWORD'),
        'HOST': config('POSTGRES_HOST'),
        'PORT': config('POSTGRES_PORT', cast=int),
    },
}
# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

#######


AUTH_USER_MODEL = 'authc.User'

# Permite desactivar CORS y aceptar conexiones desde cualquier sitio
CORS_ORIGIN_ALLOW_ALL = True

# aceptar estos headers en cross
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'context-front',
)

# Valid context fronts
CONTEXT_FRONT_MAKERS = 'm'
CONTEXT_FRONT_LOGISTICA = 'l'
CONTEXT_FRONTS = {
    CONTEXT_FRONT_MAKERS: {
        'PROTOCOL': config('MAKERS_FRONT_PROTOCOL'),
        'DOMAIN': config('MAKERS_FRONT_DOMAIN'),
        'PORT': config('MAKERS_FRONT_PORT'),
        'BASEPATH': config('MAKERS_FRONT_BASEPATH'),
    },
    CONTEXT_FRONT_LOGISTICA: {
        'PROTOCOL': config('LOGISTICA_FRONT_PROTOCOL'),
        'DOMAIN': config('LOGISTICA_FRONT_DOMAIN'),
        'PORT': config('LOGISTICA_FRONT_PORT'),
        'BASEPATH': config('LOGISTICA_FRONT_BASEPATH'),
    },
}


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
        # 'rest_framework.permissions.DjangoObjectPermissions'
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # 'rest_framework.authentication.SessionAuthentication',
        # 'rest_framework.authentication.BasicAuthentication',
        # Sólo JWT
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend', ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100,
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    )
}


SIMPLE_JWT = {
    'AUTH_HEADER_TYPES': (
        'Bearer',
        'JWT',
    ),
    'ACCESS_TOKEN_LIFETIME': timedelta(days=2),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=60),
}

SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header',
        }
    },
    'DEFAULT_INFO': 'main.urls.OPENAPI_INFO'
}

DJOSER = {
    'SEND_ACTIVATION_EMAIL': config('SEND_ACTIVATION_EMAIL', cast=bool),
    'PASSWORD_CHANGED_EMAIL_CONFIRMATION': config('PASSWORD_CHANGED_EMAIL_CONFIRMATION', cast=bool),
    'USERNAME_CHANGED_EMAIL_CONFIRMATION': config('USERNAME_CHANGED_EMAIL_CONFIRMATION', cast=bool),

    # Esta config no proviene de la librería de djoser, se trata de un
    # hack realizado en la app auth para dar soporte a distintos
    # frontends con un mismo backend
    'PASSWORD_RESET_CONFIRM_URL': '',  # Nada, se gestiona por contexto
    'USERNAME_RESET_CONFIRM_URL': '',  # Nada, se gestiona por contexto
    'ACTIVATION_URL': '',  # Nada, se gestiona por contexto
    'PASSWORD_RESET_CONFIRM_URLS': {
        CONTEXT_FRONT_MAKERS: '',
        CONTEXT_FRONT_LOGISTICA: 'recoverpass/confirm/{uid}/{token}',
    },
    'USERNAME_RESET_CONFIRM_URLS': {
        CONTEXT_FRONT_MAKERS: '',
        CONTEXT_FRONT_LOGISTICA: 'recoverusername/confirm/{uid}/{token}',
    },
    'ACTIVATION_URLS': {
        CONTEXT_FRONT_MAKERS: '',
        CONTEXT_FRONT_LOGISTICA: 'register/activate/{uid}/{token}',
    },
    'EMAIL': {
        'activation': 'authc.email.ActivationEmail',
        'confirmation': 'authc.email.ConfirmationEmail',
        'password_reset': 'authc.email.PasswordResetEmail',
        'password_changed_confirmation': 'authc.email.PasswordChangedConfirmationEmail',
        'username_changed_confirmation': 'authc.email.UsernameChangedConfirmationEmail',
        'username_reset': 'authc.email.UsernameResetEmail',
    }
}

# Debug de mail en archivos:
EMAIL_BACKEND = config('EMAIL_BACKEND')
# En caso de usar el backend file, los mails se guardarán aquí
EMAIL_FILE_PATH = "/tmp"

# En caso de usar el backend smtp real, necesitamos esta config:
# The host to use for sending email.
EMAIL_HOST = config('EMAIL_HOST')
# Password to use for the SMTP server defined in EMAIL_HOST
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
# Username to use for the SMTP server defined in EMAIL_HOST
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
# Port to use for the SMTP server defined in EMAIL_HOST.
EMAIL_PORT = config('EMAIL_PORT', cast=int)
# Whether to use a TLS (secure) connection when talking to the SMTP server. This
# is used for explicit TLS connections, generally on port 587. If you are
# experiencing hanging connections, see the implicit TLS setting EMAIL_USE_SSL.
EMAIL_USE_TLS = config('EMAIL_USE_TLS', cast=bool)
# Default from used in email sent
DEFAULT_FROM_EMAIL = config('EMAIL_HOST_USER')

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]

# Debug sql
# if DEBUG:
#     INSTALLED_APPS.append("silk")
#     MIDDLEWARE.append('silk.middleware.SilkyMiddleware')
#     SILKY_PYTHON_PROFILER = True
#     SILKY_META = True
