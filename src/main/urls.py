"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import url, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

OPENAPI_INFO = openapi.Info(
    title="COV19 Software Logistica API REST",
    default_version='v1',
    description="",
    terms_of_service="",
    contact=openapi.Contact(email="cov19@example.com"),
    license=openapi.License(name="MIT"),
)

schema_view = get_schema_view(
    OPENAPI_INFO,
    validators=['flex', 'ssv'],
    public=True,
    permission_classes=(AllowAny, ),
)

urlpatterns = [
    path('', include('front.urls')),
    url(
        r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0),
        name='schema-json',
    ),
    url(
        r'^swagger/$',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui',
    ),
    url(
        r'^redoc/$',
        schema_view.with_ui('redoc', cache_timeout=0),
        name='schema-redoc',
    ),
    url(
        r'^apiv1/auth/',
        include('djoser.urls'),
    ),
    url(
        r'^apiv1/auth/',
        include('djoser.urls.jwt'),
    ),
    url(
        r'^apiv1/logistica/basic/',
        include('logistica.urls_basic'),
    ),
    url(
        r'^apiv1/logistica/middle/',
        include('logistica.urls_middle'),
    ),
    url(
        r'^apiv1/logistica/corn/',
        include('logistica.urls_corn'),
    ),
    url(
        r'^apiv1/logistica/stats/',
        include('logistica.urls_stats'),
    ),
    path('admin/', admin.site.urls),
    url(r'^admin/dynamic_raw_id/', include('dynamic_raw_id.urls')),
]

if 'silk' in settings.INSTALLED_APPS:
    urlpatterns = [
        path('silk/', include('silk.urls', namespace='silk')),
    ] + urlpatterns
