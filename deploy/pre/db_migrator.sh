#!/bin/sh

echo "Running Django migrations..."
cd /var/www/webapp/src
python3 manage.py migrate --noinput && echo "OK" || echo "KO" ; exit
