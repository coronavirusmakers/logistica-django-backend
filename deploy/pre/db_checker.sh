#!/bin/sh

echo "Checking if Django needs to run migrations..."
cd /var/www/webapp/src
python3 manage.py shell -c 'from django.core.management.commands.showmigrations import Command, DEFAULT_DB_ALIAS, connections; import io; jj=io.StringIO(); Command(jj).show_list(connections[DEFAULT_DB_ALIAS]); jj.seek(0); exit (any("[ ]" in s for s in jj.readlines()))' || echo "Required"
