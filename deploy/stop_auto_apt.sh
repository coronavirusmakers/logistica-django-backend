#!/bin/bash

sudo systemctl stop apt-daily.service
sudo systemctl kill --kill-who=all apt-daily.service

i=0
while ! (systemctl list-units --all apt-daily.service | egrep -q '(dead|failed)'); do
  if [[ "$i" -gt 60 ]]; then
       exit 1
  fi
  sleep 1;
  ((i++))
done
