# Changelog

## [Unreleased]

- TODO

## [0.6] - 2020-03-26

### Added

- Endpoints de logística (request, shipping, carrier)
- Unit tests
- Configuración global
- Límite de Inventory (lotes) al día (configurable)
- Nested objects en serializers de person para makers
- Rol inventory con limitación de acceso por región

### Changed

- Carrier pasado a modelo Person

### Fixed

- Arreglado problema con headers cors

### Security

- Fallo de seguridad que provocaría escalada de privilegios

## [0.5] - 2020-03-23

### Added

- Basic endpoints for Person, Inventory, Resource, Region
- Stock control

### Changed

- Models refactor, use Person+Role for consumers/makers/etc
- Match admin against new models

## [0.4] - 2020-03-22

### Added

- admin StackedInline to TabularInline
- JWT
- user endpoints

### Removed

- Remove grapelli admin (use vanilla django admin)

### Security

- Security item

## [0.3] - 2020-03-21

### Added

- django admin improved
- tracking models
- Producer telegram id and nick
- Carrier model
- Request model

## [0.2] - 2020-03-20

### Added

- Some base models
- Install instructions

## [0.1] - 2020-03-19

### Added

- Project start as POC. Implemented PostGIS and Django Rest Framework
