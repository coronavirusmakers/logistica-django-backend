# CV19_makers_soft_django POC

Fase alpha de aplicación para logística.

Tecnologías utilizadas:

* python 3.7
* Django 2.2.11 (Por ser una LTS)
* Django Rest Framework (Para exponer los datos en una API con JSON)
* PostgreSQL + PostGIS (Para tareas geoespaciales)

# Documentación de la API

* Al arrancar el proyecto cond ocker se puede acceder a `/swagger/` o `/redoc/` que contiene la documentación de los endpoint disponibles


# Arrancar entorno de desarrollo

El sistema propuesto para desarrolo en local es Docker.

* Ejecutar docker-compose up
* Localizar el contenedor de django: `docker ps`

```
CONTAINER ID        IMAGE                                                 COMMAND                   CREATED             STATUS              PORTS                                               NAMES
b1f3325852cf        logistica-django-backend_django_service                "/bin/bash -c '\n    …"   17 hours ago        Up 17 hours         0.0.0.0:8080->80/tcp                                logistica-django-backend_django_service_1
```

* Entrar dentro del contenedor para aplciar las migraciones: `docker exec -it b1f3325852cf bash`
* Una vez dentro, aplicar las migraciones: `python manage migrate`
* Crear un usuario administrador: `python manage createsuperuser`

# Administración de django

La url de acceso para la admin es http://localhost:8080/admin/ y las credenciales son las indicadas con el comando indicado en "Arrancar entorno de desarrollo" (`python manage createsuperuser`).


# Instalación en producción

Los archivos de configuración de ejemplo asumen que la aplicación será instalada en `/var/www/webapp/`. Dentro de `resources/` se encuentran archivos de configuración para nginx y uwsgi.
Los parámetros de configuración de credenciales se encuentran en el archivo .env que deberá ser movido a `src/`. Durante el desarrollo este archivo permanece en un nivel superior del árbol de directorios ya que se comparte con docker.


# Desarrollo

Se utilizará el esquema propuesto por Git-Flow con rama `master` y `develop`. Para implementación de nuevas funcionalidades se utilizará `feature/feature-name`.


# Generación de datos geoespaciales

Para el desarrollo de la aplicación se ha utilizado el dataset de municipios españoles disponible en https://opendata.esri.es/datasets/53229f5912e04f1ba6dddb70a5abeb72_0
Aquí describiremos la forma de proceder para obtener, modificar y cargar otros datos

1) Obtener el dataset (.dbf, .prj, shp, shx)
2) Generar el modelo de base de datos

```
# python manage.py ogrinspect geospatialdata/Municipios_IGN/Municipios_IGN.shp  Region --srid=4326 --mapping --multi
# This is an auto-generated Django model module created by ogrinspect.
from django.contrib.gis.db import models


class Region(models.Model):
    objectid = models.BigIntegerField()
    inspireid = models.CharField(max_length=80)
    natcode = models.CharField(max_length=80)
    nameunit = models.CharField(max_length=81)
    codnut1 = models.CharField(max_length=80)
    codnut2 = models.CharField(max_length=80)
    codnut3 = models.CharField(max_length=80)
    codigoine = models.CharField(max_length=80)
    shape__are = models.FloatField()
    shape__len = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)


# Auto-generated `LayerMapping` dictionary for Region model
region_mapping = {
    'objectid': 'OBJECTID',
    'inspireid': 'INSPIREID',
    'natcode': 'NATCODE',
    'nameunit': 'NAMEUNIT',
    'codnut1': 'CODNUT1',
    'codnut2': 'CODNUT2',
    'codnut3': 'CODNUT3',
    'codigoine': 'CODIGOINE',
    'shape__are': 'Shape__Are',
    'shape__len': 'Shape__Len',
    'geom': 'MULTIPOLYGON',
}
```

3) Ajustar la clase de Region y el mapeo de fields de `region_mapping` según necesidades
4) `region_mapping` debe colocarse dentro de la clase Region
5) crear las migraciones necesarias `python manage.py makemigrations` `python manage.py migrate`
6) Cargar los datos con `python manage.py loadgeospatialdata --verbose --shpfile ruta_del_archivo.shp`


# Ejemplo de extracción de postgis a datos shape:
```
pgsql2shp -f coronavirusmakers_postalcodes -h localhost -u logistica -P logistica logistica "SELECT name, postal_code, ine_code, geom FROM logistica_region"
```

# Carga de datos geoespaciales adaptados por CoronavirusMakers

1) Obtener el zip coronavirusmakers_postalcodes.zip
2) Descomprimir
3) Cargar los datos: `python manage.py loadgeospatialdata --verbose --shpfile coronavirusmakers_postalcodes.shp`
4) No volver a ejecutar este comando PELIGRO DE DUPLICAR ENTRADAS EN LA DB
